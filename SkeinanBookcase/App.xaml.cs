﻿using Skeinan.Bookcase.DependencyFactories;
using Skeinan.Bookcase.Services;
using System.IO;
using System.Windows;

namespace Skeinan.Bookcase {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
		private void Application_Startup(object sender, StartupEventArgs e) {
			PathService pathService = new PathService();

			if (!Directory.Exists(pathService.AppDataPath)) {
				Directory.CreateDirectory(pathService.AppDataPath);
			}

			BookEditorFactory bookEditorFactory = new BookEditorFactory(pathService);
			StudentEditorFactory studentEditorFactory = new StudentEditorFactory(pathService);
			LoanEditorFactory loanEditorFactory = new LoanEditorFactory(pathService);
			AuthorEditorFactory authorEditorFactory = new AuthorEditorFactory(pathService);
			ClassificationEditorFactory classificationEditorFactory = new ClassificationEditorFactory(pathService);
			PublisherEditorFactory publisherEditorFactory = new PublisherEditorFactory(pathService);
			TitleEditorFactory titleEditorFactory = new TitleEditorFactory(pathService);
			BookSearchFactory bookSearchFactory = new BookSearchFactory(pathService);
			StudentSearchFactory studentSearchFactory = new StudentSearchFactory(pathService);
			LoanSearchFactory loanSearchFactory = new LoanSearchFactory(pathService);

			MainWindowFactory mainWindowFactory = new MainWindowFactory(
					bookEditorFactory,
					studentEditorFactory,
					loanEditorFactory,
					authorEditorFactory,
					classificationEditorFactory,
					publisherEditorFactory,
					titleEditorFactory,
					bookSearchFactory,
					studentSearchFactory,
					loanSearchFactory);

			LoginFactory loginFactory = new LoginFactory(pathService, mainWindowFactory);
			loginFactory.Create().Show();
		}
	}
}
