﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;
using System.Linq;

namespace Skeinan.Bookcase.Mappers {
	public class MinorBookMapper {
		public static MinorBookMapper Instance { get; } = new MinorBookMapper();

		public MinorBook MapToModel(BookEntity source) {
			MinorBook book = new MinorBook();
			book.Title = string.Join(Properties.Resources.ItemSeparator, source.Titles.Select(e => e.Title.Name));
			return MapperUtil.Map(source, book);
		}
	}
}
