﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;
using System.Linq;

namespace Skeinan.Bookcase.Mappers {
	class CompleteBookMapper {
		public static CompleteBookMapper Instance { get; } = new CompleteBookMapper();

		public CompleteBook MapToModel(BookEntity source) {
			CompleteBook book = MapperUtil.Map(source, new CompleteBook());
			source.Titles.ToList().ForEach(e => book.Titles.Add(TitleMapper.Instance.MapToModel(e.Title)));
			source.Authors.ToList().ForEach(e => book.Authors.Add(AuthorMapper.Instance.MapToModel(e.Author)));
			source.Classifications.ToList().ForEach(e => book.Classifications.Add(ClassificationMapper.Instance.MapToModel(e.Classification)));
			source.Publishers.ToList().ForEach(e => book.Publishers.Add(PublisherMapper.Instance.MapToModel(e.Publisher)));
			return book;
		}

		public BookEntity MapToEntity(CompleteBook source, BookEntity entity) {
			BookEntity bookEntity = MapperUtil.Map(source, entity);

			bookEntity.Titles.Clear();
			source.Titles.Where(e => e != null).ToList().ForEach(e => AddTitleReference(bookEntity, e));

			bookEntity.Authors.Clear();
			source.Authors.Where(e => e != null).ToList().ForEach(e => AddAuthorReference(bookEntity, e));

			bookEntity.Classifications.Clear();
			source.Classifications.Where(e => e != null).ToList().ForEach(e => AddClassificationReference(bookEntity, e));

			bookEntity.Publishers.Clear();
			source.Publishers.Where(e => e != null).ToList().ForEach(e => AddPublisherReference(bookEntity, e));

			return entity;
		}

		public BookEntity MapToEntity(CompleteBook source) {
			return MapToEntity(source, new BookEntity());
		}

		private void AddTitleReference(BookEntity bookEntity, Title title) {
			BookTitleEntity reference = new BookTitleEntity {
				BookId = bookEntity.BookId,
				TitleId = TitleMapper.Instance.MapToEntity(title).TitleId
			};
			bookEntity.Titles.Add(reference);
		}

		private void AddAuthorReference(BookEntity bookEntity, Author author) {
			BookAuthorEntity reference = new BookAuthorEntity {
				BookId = bookEntity.BookId,
				AuthorId = AuthorMapper.Instance.MapToEntity(author).AuthorId
			};
			bookEntity.Authors.Add(reference);
		}

		private void AddClassificationReference(BookEntity bookEntity, Classification classification) {
			BookClassificationEntity reference = new BookClassificationEntity {
				BookId = bookEntity.BookId,
				ClassificationId = ClassificationMapper.Instance.MapToEntity(classification).ClassificationId
			};
			bookEntity.Classifications.Add(reference);
		}

		private void AddPublisherReference(BookEntity bookEntity, Publisher publisher) {
			BookPublisherEntity reference = new BookPublisherEntity {
				BookId = bookEntity.BookId,
				PublisherId = PublisherMapper.Instance.MapToEntity(publisher).PublisherId
			};
			bookEntity.Publishers.Add(reference);
		}
	}
}
