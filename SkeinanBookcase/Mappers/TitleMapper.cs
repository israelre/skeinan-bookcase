﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class TitleMapper {
		public static TitleMapper Instance { get; } = new TitleMapper();

		public Title MapToModel(TitleEntity source) {
			return MapperUtil.Map(source, new Title());
		}

		public TitleEntity MapToEntity(Title source, TitleEntity target) {
			return MapperUtil.Map(source, target);
		}

		public TitleEntity MapToEntity(Title source) {
			return MapToEntity(source, new TitleEntity());
		}
	}
}
