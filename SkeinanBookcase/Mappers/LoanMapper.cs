﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class LoanMapper {
		public static LoanMapper Instance { get; } = new LoanMapper();

		public Loan MapToModel(LoanEntity source) {
			return MapperUtil.Map(source, new Loan());
		}

		public LoanEntity MapToEntity(Loan source, LoanEntity target) {
			return MapperUtil.Map(source, target);
		}

		public LoanEntity MapToEntity(Loan source) {
			return MapToEntity(source, new LoanEntity());
		}
	}
}
