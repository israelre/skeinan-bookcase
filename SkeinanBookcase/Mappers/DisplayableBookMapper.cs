﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;
using System.Linq;

namespace Skeinan.Bookcase.Mappers {
	public class DisplayableBookMapper {
		public static DisplayableBookMapper Instance { get; } = new DisplayableBookMapper();

		public DisplayableBook MapToModel(BookEntity source) {
			DisplayableBook book = MapperUtil.Map(source, new DisplayableBook());
			book.Title = string.Join(Properties.Resources.ItemSeparator, source.Titles.Select(e => e.Title.Name));
			book.Author = string.Join(Properties.Resources.ItemSeparator, source.Authors.Select(e => e.Author.Name));
			book.Classification = string.Join(Properties.Resources.ItemSeparator, source.Classifications.Select(e => e.Classification.Name));
			book.Publisher = string.Join(Properties.Resources.ItemSeparator, source.Publishers.Select(e => e.Publisher.Name));
			book.LoanDate = source.Loans.Where(e => e.ReturnDate == null).FirstOrDefault()?.LoanDate;
			return book;
		}
	}
}
