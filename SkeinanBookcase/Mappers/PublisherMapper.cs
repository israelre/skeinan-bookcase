﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class PublisherMapper {
		public static PublisherMapper Instance { get; } = new PublisherMapper();

		public Publisher MapToModel(PublisherEntity source) {
			return MapperUtil.Map(source, new Publisher());
		}

		public PublisherEntity MapToEntity(Publisher source, PublisherEntity target) {
			return MapperUtil.Map(source, target);
		}

		public PublisherEntity MapToEntity(Publisher source) {
			return MapToEntity(source, new PublisherEntity());
		}
	}
}
