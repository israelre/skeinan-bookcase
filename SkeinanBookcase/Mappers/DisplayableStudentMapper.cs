﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;
using System.Collections.Generic;
using System.Linq;

namespace Skeinan.Bookcase.Mappers {
	public class DisplayableStudentMapper {
		public static DisplayableStudentMapper Instance { get; } = new DisplayableStudentMapper();

		public DisplayableStudent MapToModel(StudentEntity source) {
			DisplayableStudent student = MapperUtil.Map(source, new DisplayableStudent());
			ICollection<BookTitleEntity> titles = source.Loans.Where(e => e.ReturnDate == null).FirstOrDefault()?.Book.Titles;
			if (titles != null) student.BookTitle = string.Join(Properties.Resources.ItemSeparator, titles.Select(e => e.Title.Name));
			return student;
		}
	}
}
