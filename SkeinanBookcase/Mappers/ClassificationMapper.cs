﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class ClassificationMapper {
		public static ClassificationMapper Instance { get; } = new ClassificationMapper();

		public Classification MapToModel(ClassificationEntity source) {
			return MapperUtil.Map(source, new Classification());
		}

		public ClassificationEntity MapToEntity(Classification source, ClassificationEntity target) {
			return MapperUtil.Map(source, target);
		}

		public ClassificationEntity MapToEntity(Classification source) {
			return MapToEntity(source, new ClassificationEntity());
		}
	}
}
