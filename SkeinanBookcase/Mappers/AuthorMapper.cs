﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class AuthorMapper {
		public static AuthorMapper Instance { get; } = new AuthorMapper();

		public Author MapToModel(AuthorEntity source) {
			return MapperUtil.Map(source, new Author());
		}

		public AuthorEntity MapToEntity(Author source, AuthorEntity target) {
			return MapperUtil.Map(source, target);
		}

		public AuthorEntity MapToEntity(Author source) {
			return MapToEntity(source, new AuthorEntity());
		}
	}
}
