﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;
using System.Linq;

namespace Skeinan.Bookcase.Mappers {
	public class DisplayableLoanMapper {
		public static DisplayableLoanMapper Instance { get; } = new DisplayableLoanMapper();

		public DisplayableLoan MapToModel(LoanEntity source) {
			DisplayableLoan loan = MapperUtil.Map(source, new DisplayableLoan());
			loan.BookTitle = string.Join(Properties.Resources.ItemSeparator, source.Book.Titles.Select(e => e.Title.Name));
			loan.StudentName = source.Student.Name;
			return loan;
		}
	}
}
