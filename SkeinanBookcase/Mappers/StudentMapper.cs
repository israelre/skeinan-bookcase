﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class StudentMapper {
		public static StudentMapper Instance { get; } = new StudentMapper();

		public Student MapToModel(StudentEntity source) {
			return MapperUtil.Map(source, new Student());
		}

		public StudentEntity MapToEntity(Student source, StudentEntity target) {
			return MapperUtil.Map(source, target);
		}

		public StudentEntity MapToEntity(Student source) {
			return MapToEntity(source, new StudentEntity());
		}
	}
}
