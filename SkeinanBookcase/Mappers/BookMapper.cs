﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class BookMapper {
		public static BookMapper Instance { get; } = new BookMapper();

		public Book MapToModel(BookEntity source) {
			return MapperUtil.Map(source, new Book());
		}
	}
}
