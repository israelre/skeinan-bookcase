﻿using System.Reflection;
using System.Linq;
using System;

namespace Skeinan.Bookcase.Mappers {
	class MapperUtil {
		public static T Map<T>(object source, T target) {
			if (source == null) return default(T);
			foreach (PropertyInfo targetProperty in target.GetType().GetProperties().Where(p => p.CanWrite)) {
				PropertyInfo sourceProperty = source.GetType().GetProperty(targetProperty.Name);
				if (sourceProperty != null && sourceProperty.PropertyType == targetProperty.PropertyType) {
					string name = sourceProperty.Name;
					Type sourceT = sourceProperty.PropertyType;
					Type targetT = targetProperty.PropertyType;
					targetProperty.SetValue(target, sourceProperty.GetValue(source));
				}
			}
			return target;
		}
	}
}
