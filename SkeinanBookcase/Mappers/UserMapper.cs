﻿using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Mappers {
	public class UserMapper {
		public static UserMapper Instance { get; } = new UserMapper();

		public User MapToModel(UserEntity source) {
			return MapperUtil.Map(source, new User());
		}

		public UserEntity MapToEntity(User source, UserEntity target) {
			return MapperUtil.Map(source, target);
		}

		public UserEntity MapToEntity(User source) {
			return MapToEntity(source, new UserEntity());
		}
	}
}
