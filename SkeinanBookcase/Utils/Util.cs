﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace Skeinan.Bookcase.Utils {
	static class Util {
		public static string BoolToLocalizedString(bool? value) {
			if (value == null) return null;
			return value == true ? Properties.Resources.Yes : Properties.Resources.No;
		}

		public static string LocalizedDateFormat {
			get {
				return DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
			}
		}

		public static string FormatDate(DateTime? date) {
			if (date == null) {
				return Properties.Resources.NotApplicable;
			} else {
				return date.Value.ToString(LocalizedDateFormat, CultureInfo.CurrentCulture);
			}
		}

		public static string FormatDateTime(DateTime? date) {
			if (date == null) {
				return Properties.Resources.NotApplicable;
			} else {
				return date.Value.ToString(LocalizedDateTimeFormat, CultureInfo.CurrentCulture);
			}
		}

		public static string LocalizedDateTimeFormat {
			get {
				return string.Format(CultureInfo.CurrentCulture, "{0} {1}", DateTimeFormatInfo.CurrentInfo.ShortDatePattern, DateTimeFormatInfo.CurrentInfo.LongTimePattern);
			}
		}

		public static bool IsEqual(object o1, object o2) {
			return o1 == null || o2 == null ? o1 == o2 : o1.GetType() == o2.GetType() ? o1.Equals(o2) : false;
		}

		public static bool ArePropertiesEqual(object o1, object o2) {
			if (o1.GetType() != o2.GetType()) return false;
			if (o1 == null || o2 == null) return o1 == o2;
			return o1.GetType().GetProperties().ToList().All(p => IsEqual(p.GetValue(o1), p.GetValue(o2)));
		}

		public static int? ToInt(string value) {
			int result;
			if (int.TryParse(value, out result)) {
				return result;
			} else {
				return null;
			}
		}

		public static void RefreshCompleteList<T, U>(BindingList<T> completeList, IEnumerable<U> retrievedList, Func<U, T> mapper) {
			completeList.RaiseListChangedEvents = false;
			completeList.Clear();
			retrievedList.ToList().ForEach(e => completeList.Add(mapper(e)));

			completeList.RaiseListChangedEvents = true;
			completeList.ResetBindings();
		}

		public static void RefreshCompleteList<T, U>(BindingList<T> completeList, IEnumerable<U> retrievedList, Func<U, T> mapper,
				BindingList<T> selectedList, Func<T, long?> retrieveId) {
			List<long?> selectedIds = null;
			if (selectedList != null) {
				selectedList.RaiseListChangedEvents = false;
				selectedIds = selectedList.Select(retrieveId).ToList();
			}

			RefreshCompleteList(completeList, retrievedList, mapper);

			if (selectedList != null) {
				selectedList.Clear();
				selectedIds.ForEach(id => selectedList.Add(completeList.Where(e => retrieveId(e) == id).FirstOrDefault()));
				selectedList.RaiseListChangedEvents = true;
				selectedList.ResetBindings();
			}
		}
	}
}
