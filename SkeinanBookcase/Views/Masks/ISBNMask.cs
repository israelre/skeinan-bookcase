﻿using SkeinanWpf.Masks;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views.Masks {
	class IsbnMask : Mask {
		public IsbnMask(TextBox textBox) : base(textBox) {
		}

		protected override bool Validate(string text) {
			if (!Regex.IsMatch(text, "^[\\d]*[Xx]?$")) {
				return false;
			} else if (Regex.Replace(text, "[^\\dXx]", "").Length > 13) {
				return false;
			} else {
				return true;
			}
		}
	}
}
