﻿using SkeinanWpf;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views {
	public partial class ClassificationEditorView : TabContent {
		public ClassificationService Logic { get; }

		public ClassificationEditorView(ClassificationService logic) {
			Logic = logic;
			InitializeComponent();
			PostInitializeComponent();
			Loaded += ClassificationEditorView_Loaded;
			ChangeButton.Click += ChangeButton_Click;
		}

		private void ClassificationEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad();
			BindAndMask();
		}

		private void BindAndMask() {
			OldNameComboBox.ItemsSource = Logic.Classifications;
			OldNameComboBox.DisplayMemberPath = nameof(Logic.Classification.Name);
			BindingManager oldNameBinding = new BindingManager(OldNameComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.Classification)) {
				ValidationResultElement = OldNameValidationLabel,
			};
			oldNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(oldNameBinding);

			BindingManager newNameBinding = new BindingManager(NewNameTextBox, TextBox.TextProperty, Logic, nameof(Logic.NewName)) {
				ValidationResultElement = NewNameValidationLabel
			};
			newNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(newNameBinding);
		}

		private void ChangeButton_Click(object sender, RoutedEventArgs e) {
			if (Validate(Properties.Resources.ChangeInvalid, Properties.Resources.ChangeClassification)
					&& Save(Properties.Resources.ChangeSuccess, Properties.Resources.ChangeClassification, Logic.Save)) {
				OnClose();
			}
		}
	}
}
