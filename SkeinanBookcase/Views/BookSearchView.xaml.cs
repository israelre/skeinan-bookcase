﻿using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Models;
using SkeinanWpf;
using SkeinanWpf.Tabs;
using System.Windows.Input;
using System.Windows.Media;

namespace Skeinan.Bookcase.Views {
	public partial class BookSearchView : TabContent {
		public ActionCommand<long> LoanCommand { get; set; }
		public ActionCommand<long> ChangeCommand { get; set; }

		public BookSearchService Logic { get; }
		public ObservableCollection<DisplayableBook> GridBooks { get; set; } = new ObservableCollection<DisplayableBook>();

		private bool mIsGridEnabled;
		public bool IsGridEnabled {
			get { return mIsGridEnabled; }
			set { mIsGridEnabled = value; NotifyPropertyChanged(); }
		}

		public BookSearchView(BookSearchService logic) {
			Logic = logic;

			Logic.ResultData.ListChanged += OnBooksChanged;

			InitializeComponent();

			CreateDataGrid();

			BindSearch(CodeTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Code));
			BindSearch(IsbnTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Isbn));
			BindSearch(YearTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Year));
			BindSearch(EditionTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Edition));
			BindSearch(SourceTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Source));
			BindSearch(TitleTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Title));
			BindSearch(AuthorTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Author));
			BindSearch(ClassificationTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Classification));
			BindSearch(PublisherTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Publisher));
			AddBindingManager(new BindingManager(BooksDataGrid, ItemsControl.IsEnabledProperty, this, nameof(IsGridEnabled)));
			AddBindingManager(new BindingManager(PageNumberLabel, Label.ContentProperty, Logic, nameof(Logic.PageNumber)) {
				Mode = BindingMode.OneWay,
				Converter = new PageNumberToStringConverter()
			});

			AddBindingManager(new BindingManager(BottomPreviousButton, Button.VisibilityProperty, Logic, nameof(Logic.HasPreviousPage)) {
				Converter = new BooleanToVisibilityConverter()
			});
			AddBindingManager(new BindingManager(BottomNextButton, Button.VisibilityProperty, Logic, nameof(Logic.HasNextPage)) {
				Converter = new BooleanToVisibilityConverter()
			});

			Loaded += BookSearchView_Loaded;
			SearchButton.Click += SearchButton_Click;
			BottomPreviousButton.Click += (sender, e) => Logic.PagePrevious();
			BottomNextButton.Click += (sender, e) => Logic.PageNext();
			AccessKeyManager.AddAccessKeyPressedHandler(this, AccessKeyPressed);

			PostInitializeComponent();
		}

		private void SearchButton_Click(object sender, RoutedEventArgs e) {
			Logic.Search();
		}

		private void BookSearchView_Loaded(object sender, RoutedEventArgs e) {
			LoanCommand = new ActionCommand<long>(this.GetWindow().OpenAddLoanForBook);
			ChangeCommand = new ActionCommand<long>(this.GetWindow().OpenAddBook);
			IsGridEnabled = false;
			Logic.OnLoad();
		}

		public void AccessKeyPressed(object sender, AccessKeyPressedEventArgs e) {
			if (e.Key != null && Keyboard.FocusedElement is FrameworkElement) {
				GetBindingManager(Keyboard.FocusedElement as FrameworkElement)?.UpdateBinding();
			}
		}

		private void BindSearch(FrameworkElement element, DependencyProperty property, string field, UpdateSourceTrigger updateSourceTrigger = UpdateSourceTrigger.LostFocus) {
			AddBindingManager(new BindingManager(element, property, Logic, string.Join(".", nameof(Logic.CurrentSearch), field)) {
				UpdateSourceTrigger = updateSourceTrigger
			});
		}

		private void CreateDataGrid() {
			SetupDataGrid(BooksDataGrid);
			BooksDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Code, nameof(DisplayableBook.Code)));
			BooksDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Name, nameof(DisplayableBook.Title)));
			
			BooksDataGrid.Columns.Add(CreateDataGridActionColumn("", Properties.Resources.Change, nameof(ChangeCommand), new Binding(nameof(Book.BookId))));
			BooksDataGrid.Columns.Add(CreateDataGridActionColumn("", Properties.Resources.LoanReturn, nameof(LoanCommand), new Binding(nameof(Book.BookId))));
		}

		private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e) {
			ScrollViewer scrollViewer = (ScrollViewer)sender;
			scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta);
			e.Handled = true;
		}

		private void OnBooksChanged(object sender, ListChangedEventArgs e) {
			Application.Current.Dispatcher.Invoke(() => {
				BindingOperations.SetBinding(BooksDataGrid, ItemsControl.ItemsSourceProperty, new Binding());

				GridBooks.Clear();
				Logic.ResultData.ToList().ForEach(GridBooks.Add);
				AddBindingManager(BooksDataGrid, ItemsControl.ItemsSourceProperty, this, nameof(GridBooks));

				IsGridEnabled = true;
			});
		}
	}
}
