﻿using SkeinanWpf;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views {
	public partial class TitleEditorView : TabContent {
		public TitleService Logic { get; }

		public TitleEditorView(TitleService logic) {
			Logic = logic;
			InitializeComponent();
			PostInitializeComponent();
			Loaded += TitleEditorView_Loaded;
			ChangeButton.Click += ChangeButton_Click;
		}

		private void TitleEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad();
			BindAndMask();
		}

		private void BindAndMask() {
			OldNameComboBox.ItemsSource = Logic.Titles;
			OldNameComboBox.DisplayMemberPath = nameof(Logic.Title.Name);
			BindingManager oldNameBinding = new BindingManager(OldNameComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.Title)) {
				ValidationResultElement = OldNameValidationLabel,
			};
			oldNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(oldNameBinding);

			BindingManager newNameBinding = new BindingManager(NewNameTextBox, TextBox.TextProperty, Logic, nameof(Logic.NewName)) {
				ValidationResultElement = NewNameValidationLabel
			};
			newNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(newNameBinding);
		}

		private void ChangeButton_Click(object sender, RoutedEventArgs e) {
			if (Validate(Properties.Resources.ChangeInvalid, Properties.Resources.ChangeTitle)
					&& Save(Properties.Resources.ChangeSuccess, Properties.Resources.ChangeTitle, Logic.Save)) {
				OnClose();
			}
		}
	}
}
