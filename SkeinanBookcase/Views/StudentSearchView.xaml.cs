﻿using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Models;
using SkeinanWpf;
using SkeinanWpf.Tabs;
using System.Windows.Input;
using System.Windows.Media;

namespace Skeinan.Bookcase.Views {
	public partial class StudentSearchView : TabContent {
		public ActionCommand<long> LoanCommand { get; set; }
		public ActionCommand<long> ChangeCommand { get; set; }

		public StudentSearchService Logic { get; }
		public ObservableCollection<DisplayableStudent> GridStudents { get; set; } = new ObservableCollection<DisplayableStudent>();

		private bool mIsGridEnabled;
		public bool IsGridEnabled {
			get { return mIsGridEnabled; }
			set { mIsGridEnabled = value; NotifyPropertyChanged(); }
		}

		public StudentSearchView(StudentSearchService logic) {
			Logic = logic;

			Logic.ResultData.ListChanged += OnStudentsChanged;

			InitializeComponent();

			CreateDataGrid();

			new BoolItemsSource(Properties.Resources.All, Properties.Resources.Transferred, Properties.Resources.NotTransferred).Bind(TransferredComboBox);
			BindSearch(TransferredComboBox, ComboBox.SelectedValueProperty, nameof(Logic.CurrentSearch.Transferred), UpdateSourceTrigger.PropertyChanged);
			BindSearch(CodeTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Code));
			BindSearch(NameTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Name));
			BindSearch(YearTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Year));
			BindSearch(GradeTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Grade));
			BindSearch(ClassroomTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Classroom));
			BindSearch(NumberTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.Number));
			BindSearch(BirthDatePicker, DatePicker.SelectedDateProperty, nameof(Logic.CurrentSearch.BirthDate));
			AddBindingManager(new BindingManager(StudentsDataGrid, ItemsControl.IsEnabledProperty, this, nameof(IsGridEnabled)));
			AddBindingManager(new BindingManager(PageNumberLabel, Label.ContentProperty, Logic, nameof(Logic.PageNumber)) {
				Mode = BindingMode.OneWay,
				Converter = new PageNumberToStringConverter()
			});

			AddBindingManager(new BindingManager(BottomPreviousButton, Button.VisibilityProperty, Logic, nameof(Logic.HasPreviousPage)) {
				Converter = new BooleanToVisibilityConverter()
			});
			AddBindingManager(new BindingManager(BottomNextButton, Button.VisibilityProperty, Logic, nameof(Logic.HasNextPage)) {
				Converter = new BooleanToVisibilityConverter()
			});

			Loaded += StudentSearchView_Loaded;
			SearchButton.Click += SearchButton_Click;
			BottomPreviousButton.Click += (sender, e) => Logic.PagePrevious();
			BottomNextButton.Click += (sender, e) => Logic.PageNext();
			AccessKeyManager.AddAccessKeyPressedHandler(this, AccessKeyPressed);

			PostInitializeComponent();
		}

		private void SearchButton_Click(object sender, RoutedEventArgs e) {
			Logic.Search();
		}

		private void StudentSearchView_Loaded(object sender, RoutedEventArgs e) {
			LoanCommand = new ActionCommand<long>(this.GetWindow().OpenAddLoanForStudent);
			ChangeCommand = new ActionCommand<long>(this.GetWindow().OpenAddStudent);
			IsGridEnabled = false;
			Logic.OnLoad();
		}

		public void AccessKeyPressed(object sender, AccessKeyPressedEventArgs e) {
			if (e.Key != null && Keyboard.FocusedElement is FrameworkElement) {
				GetBindingManager(Keyboard.FocusedElement as FrameworkElement)?.UpdateBinding();
			}
		}

		private void BindSearch(FrameworkElement element, DependencyProperty property, string field, UpdateSourceTrigger updateSourceTrigger = UpdateSourceTrigger.LostFocus) {
			AddBindingManager(new BindingManager(element, property, Logic, string.Join(".", nameof(Logic.CurrentSearch), field)) {
				UpdateSourceTrigger = updateSourceTrigger
			});
		}

		private void CreateDataGrid() {
			SetupDataGrid(StudentsDataGrid);
			StudentsDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Code, nameof(DisplayableStudent.Code)));
			StudentsDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Name, nameof(DisplayableStudent.Name)));
			
			StudentsDataGrid.Columns.Add(CreateDataGridActionColumn("", Properties.Resources.Change, nameof(ChangeCommand), new Binding(nameof(DisplayableStudent.StudentId))));
			StudentsDataGrid.Columns.Add(CreateDataGridActionColumn("", Properties.Resources.LoanReturn, nameof(LoanCommand), new Binding(nameof(DisplayableStudent.StudentId))));
		}

		

		private void OnStudentsChanged(object sender, ListChangedEventArgs e) {
			Application.Current.Dispatcher.Invoke(() => {
				BindingOperations.SetBinding(StudentsDataGrid, ItemsControl.ItemsSourceProperty, new Binding());

				GridStudents.Clear();
				Logic.ResultData.ToList().ForEach(GridStudents.Add);
				AddBindingManager(StudentsDataGrid, ItemsControl.ItemsSourceProperty, this, nameof(GridStudents));

				IsGridEnabled = true;
			});
		}
	}
}
