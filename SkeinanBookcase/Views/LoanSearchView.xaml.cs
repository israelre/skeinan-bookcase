﻿using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Models;
using SkeinanWpf;
using SkeinanWpf.Tabs;
using System.Windows.Input;
using System.Windows.Media;
using SkeinanWpf.Converters;

namespace Skeinan.Bookcase.Views {
	public partial class LoanSearchView : TabContent {
		public ActionCommand<long> ReturnCommand { get; set; }

		public LoanSearchService Logic { get; }
		public ObservableCollection<DisplayableLoan> GridLoans { get; set; } = new ObservableCollection<DisplayableLoan>();

		private bool mIsGridEnabled;
		public bool IsGridEnabled {
			get { return mIsGridEnabled; }
			set { mIsGridEnabled = value; NotifyPropertyChanged(); }
		}

		public LoanSearchView(LoanSearchService logic) {
			Logic = logic;

			Logic.ResultData.ListChanged += OnLoansChanged;

			InitializeComponent();

			CreateDataGrid();

			BindSearch(TitleTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.BookTitle));
			BindSearch(StudentTextBox, TextBox.TextProperty, nameof(Logic.CurrentSearch.StudentName));
			BindSearch(LoanDatePicker, DatePicker.SelectedDateProperty, nameof(Logic.CurrentSearch.LoanDate));
			BindSearch(ReturnDatePicker, DatePicker.SelectedDateProperty, nameof(Logic.CurrentSearch.ReturnDate));

			AddBindingManager(new BindingManager(LoansDataGrid, ItemsControl.IsEnabledProperty, this, nameof(IsGridEnabled)));
			AddBindingManager(new BindingManager(PageNumberLabel, Label.ContentProperty, Logic, nameof(Logic.PageNumber)) {
				Mode = BindingMode.OneWay,
				Converter = new PageNumberToStringConverter()
			});

			AddBindingManager(new BindingManager(BottomPreviousButton, Button.VisibilityProperty, Logic, nameof(Logic.HasPreviousPage)) {
				Converter = new BooleanToVisibilityConverter()
			});
			AddBindingManager(new BindingManager(BottomNextButton, Button.VisibilityProperty, Logic, nameof(Logic.HasNextPage)) {
				Converter = new BooleanToVisibilityConverter()
			});

			Loaded += LoanSearchView_Loaded;
			SearchButton.Click += SearchButton_Click;
			BottomPreviousButton.Click += (sender, e) => Logic.PagePrevious();
			BottomNextButton.Click += (sender, e) => Logic.PageNext();
			AccessKeyManager.AddAccessKeyPressedHandler(this, AccessKeyPressed);

			PostInitializeComponent();
		}

		private void SearchButton_Click(object sender, RoutedEventArgs e) {
			Logic.Search();
		}

		private void LoanSearchView_Loaded(object sender, RoutedEventArgs e) {
			ReturnCommand = new ActionCommand<long>(this.GetWindow().OpenAddLoanForBook);
			IsGridEnabled = false;
			Logic.OnLoad();
		}

		public void AccessKeyPressed(object sender, AccessKeyPressedEventArgs e) {
			if (e.Key != null && Keyboard.FocusedElement is FrameworkElement) {
				GetBindingManager(Keyboard.FocusedElement as FrameworkElement)?.UpdateBinding();
			}
		}

		private void BindSearch(FrameworkElement element, DependencyProperty property, string field, UpdateSourceTrigger updateSourceTrigger = UpdateSourceTrigger.LostFocus) {
			AddBindingManager(new BindingManager(element, property, Logic, string.Join(".", nameof(Logic.CurrentSearch), field)) {
				UpdateSourceTrigger = updateSourceTrigger
			});
		}

		private void CreateDataGrid() {
			SetupDataGrid(LoansDataGrid);

			LoansDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Title, nameof(DisplayableLoan.BookTitle)));
			LoansDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.Student, nameof(DisplayableLoan.StudentName)));
			LoansDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.LoanDate, nameof(DisplayableLoan.LoanDate)));
			LoansDataGrid.Columns.Add(CreateDataGridColumn(Properties.Resources.ReturnDate, nameof(DisplayableLoan.ReturnDate)));

			Binding returnVisibilityBinding = new Binding(nameof(DisplayableLoan.ReturnDate));
			returnVisibilityBinding.Converter = new ObjectToVisibilityConverter() {
				Inverse = true
			};
			LoansDataGrid.Columns.Add(CreateDataGridActionColumn("", Properties.Resources.Return, nameof(ReturnCommand), new Binding(nameof(DisplayableLoan.BookId)), returnVisibilityBinding));
		}

		private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e) {
			ScrollViewer scrollViewer = (ScrollViewer)sender;
			scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta);
			e.Handled = true;
		}

		private void OnLoansChanged(object sender, ListChangedEventArgs e) {
			Application.Current.Dispatcher.Invoke(() => {
				BindingOperations.SetBinding(LoansDataGrid, ItemsControl.ItemsSourceProperty, new Binding());

				GridLoans.Clear();
				Logic.ResultData.ToList().ForEach(GridLoans.Add);
				AddBindingManager(LoansDataGrid, ItemsControl.ItemsSourceProperty, this, nameof(GridLoans));

				IsGridEnabled = true;
			});
		}
	}
}
