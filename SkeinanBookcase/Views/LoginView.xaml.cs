﻿using Skeinan.Bookcase.DependencyFactories;
using Skeinan.Bookcase.Services;
using System.Windows;
using System.Windows.Input;

namespace Skeinan.Bookcase.Views {
	public partial class LoginView : Window {
		public MainWindowFactory MainWindowFactory { get; }
		public LoginService Logic { get; }

		public LoginView(LoginService logic, MainWindowFactory mainWindowFactory) {
			MainWindowFactory = mainWindowFactory;
			Logic = logic;

			logic.OnLoad();

			WindowStartupLocation = WindowStartupLocation.CenterScreen;
			InitializeComponent();

			UserTextBox.Focus();
		}

		private void Login_Click(object sender, RoutedEventArgs e) {
			ExecuteLogin();
		}

		private void UserTextBox_PreviewKeyDown(object sender, KeyEventArgs e) {
			if (e.Key == Key.Enter) {
				ExecuteLogin();
			}
		}

		private void ExecuteLogin() {

			bool isAuthenticated = false;
			if (UserTextBox.Text != "") {
				Session session = new Session();
				session.User = Logic.LogIn(UserTextBox.Text.Trim(), PasswordBox.Password.ToString());
				if (session.User != null) {
					isAuthenticated = true;
					MainWindowFactory.Create(session).Show();
					Close();
				}
			}
			if (!isAuthenticated) {
				MessageBox.Show(Properties.Resources.InvalidLogin, Properties.Resources.Login, MessageBoxButton.OK, MessageBoxImage.Exclamation);
				UserTextBox.Focus();
			}
		}
	}
}
