﻿using SkeinanWpf.Tabs;
using System.Windows;

namespace Skeinan.Bookcase.Views {
    public static class BookcaseTabContent {
		public static MainWindowView GetWindow(this TabContent tabContent) {
			return (MainWindowView)Window.GetWindow(tabContent);
		}
    }
}
