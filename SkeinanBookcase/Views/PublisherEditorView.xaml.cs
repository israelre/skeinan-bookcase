﻿using SkeinanWpf;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views {
	public partial class PublisherEditorView : TabContent {
		public PublisherService Logic { get; }

		public PublisherEditorView(PublisherService logic) {
			Logic = logic;
			InitializeComponent();
			PostInitializeComponent();
			Loaded += PublisherEditorView_Loaded;
			ChangeButton.Click += ChangeButton_Click;
		}

		private void PublisherEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad();
			BindAndMask();
		}

		private void BindAndMask() {
			OldNameComboBox.ItemsSource = Logic.Publishers;
			OldNameComboBox.DisplayMemberPath = nameof(Logic.Publisher.Name);
			BindingManager oldNameBinding = new BindingManager(OldNameComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.Publisher)) {
				ValidationResultElement = OldNameValidationLabel,
			};
			oldNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(oldNameBinding);

			BindingManager newNameBinding = new BindingManager(NewNameTextBox, TextBox.TextProperty, Logic, nameof(Logic.NewName)) {
				ValidationResultElement = NewNameValidationLabel
			};
			newNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(newNameBinding);
		}

		private void ChangeButton_Click(object sender, RoutedEventArgs e) {
			if (Validate(Properties.Resources.ChangeInvalid, Properties.Resources.ChangePublisher)
					&& Save(Properties.Resources.ChangeSuccess, Properties.Resources.ChangePublisher, Logic.Save)) {
				OnClose();
			}
		}
	}
}
