﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views.Validators {
	class IsbnValidationRule : ValidationRule {
		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			string text = (string)value;
			bool valid = string.IsNullOrEmpty(text) || Regex.IsMatch(text, "^[\\dX]{10}([\\dX]{3})?$");
			return valid ? ValidationResult.ValidResult : new ValidationResult(false, Properties.Resources.InvalidIsbn);
		}
	}
}