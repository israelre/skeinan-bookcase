﻿using Skeinan.Bookcase.Services;
using System.Globalization;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views.Validators {
	class StudentCodeValidationRule : ValidationRule {
		public StudentService Logic { get; }

		public StudentCodeValidationRule(StudentService logic) {
			Logic = logic;
		}

		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			bool valid = !Logic.StudentCodeExists((string)value);
			return valid ? ValidationResult.ValidResult : new ValidationResult(false, Properties.Resources.CodeExists);
		}
	}
}
