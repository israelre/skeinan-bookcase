﻿using Skeinan.Bookcase.Services;
using System.Globalization;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views.Validators {
	class BookCodeValidationRule : ValidationRule {
		public BookService Logic { get; }

		public BookCodeValidationRule(BookService logic) {
			Logic = logic;
		}

		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			bool valid = !Logic.BookCodeExists((string)value);
			return valid ? ValidationResult.ValidResult : new ValidationResult(false, Properties.Resources.CodeExists);
		}
	}
}
