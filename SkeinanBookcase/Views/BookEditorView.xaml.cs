﻿using SkeinanWpf;
using SkeinanWpf.Fields;
using SkeinanWpf.Masks;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using Skeinan.Bookcase.Views.Masks;
using Skeinan.Bookcase.Views.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Models;
using System.Windows.Data;
using System;
using System.Windows.Input;
using SkeinanWpf.Converters;
using System.Windows.Controls;
using System.ComponentModel;
using System.Collections;

namespace Skeinan.Bookcase.Views {
	public partial class BookEditorView : TabContent {
		private bool IsInitializatedOnce { get; set; }

		public BookService Logic { get; }

		public long ExternalBookId { get; set; }

		public BookEditorView(BookService logic) {
			Logic = logic;
			InitializeComponent();
			PostInitializeComponent();
			Loaded += BookEditorView_Loaded;
			AddButton.Click += AddButton_Click;
		}

		private void AddButton_Click(object sender, RoutedEventArgs e) {
			Add(false);
		}

		private void AddCopyButton_Click(object sender, RoutedEventArgs e) {
			if (Logic.Book.BookId != 0) {
				Logic.ResetIdentification();
			} else {
				Add(true);
			}
		}

		private void Add(bool retainData) {
			bool exists = Logic.Book.BookId != 0;

			string caption = exists ? Properties.Resources.ChangeBook : Properties.Resources.AddBook;
			string validationMessage = exists ? Properties.Resources.ChangeInvalid : Properties.Resources.AddInvalid;
			string successMessage = exists ? Properties.Resources.ChangeBookSuccess : Properties.Resources.AddBookSuccess;

			if (Validate(validationMessage, caption)
					&& Save(successMessage, caption, Logic.Save, exists ? Properties.Resources.ChangeBookConfirmation : null)) {
				if (retainData) {
					Logic.ResetIdentification();
					Keyboard.Focus(CodeTextBox);
				} else if (exists) {
					OnClose();
				} else {
					IsValidationEnabled = false;
					Logic.Reset();
					IsValidationEnabled = true;
					Keyboard.Focus(CodeTextBox);
				}
			}
		}

		private void BindAndMask() {
			InitializeSuggestionFields();

			BindingManager codeBinding = CreateBookStringBinding(CodeTextBox, nameof(Logic.Book.Code));
			codeBinding.ValidationResultElement = CodeValidationLabel;
			codeBinding.ValidationRules.Add(new MandatoryValidationRule());
			codeBinding.ValidationRules.Add(new BookCodeValidationRule(Logic));
			AddBindingManager(codeBinding);

			BindingManager isbnBinding = CreateBookStringBinding(IsbnTextBox, nameof(Logic.Book.Isbn));
			isbnBinding.ValidationResultElement = IsbnValidationLabel;
			isbnBinding.ValidationRules.Add(new IsbnValidationRule());
			AddBindingManager(isbnBinding);
			new IsbnMask(IsbnTextBox).Attach();

			AddBindingManager(CreateBookStringBinding(YearTextBox, nameof(Logic.Book.Year)));
			new NumericMask(YearTextBox).Attach();

			AddBindingManager(CreateBookStringBinding(EditionTextBox, nameof(Logic.Book.Edition)));
			new NumericMask(EditionTextBox).Attach();

			AddBindingManager(CreateBookStringBinding(SourceTextBox, nameof(Logic.Book.Source)));
		}

		private void InitializeSuggestionFields() {
			InitializeSuggestionField(AuthorSuggestionField,
					Logic.QueryAuthorSuggestions,
					nameof(Author.Name),
					nameof(Logic.Book.Authors),
					true, AuthorValidationLabel, OnAuthorSelected);

			InitializeSuggestionField(ClassificationSuggestionField,
					Logic.QueryClassificationSuggestions,
					nameof(Classification.Name),
					nameof(Logic.Book.Classifications),
					true, ClassificationValidationLabel, OnClassificationSelected);

			InitializeSuggestionField(TitleSuggestionField,
					Logic.QueryTitleSuggestions,
					nameof(Title.Name),
					nameof(Logic.Book.Titles),
					true, TitleValidationLabel, OnTitleSelected);

			InitializeSuggestionField(PublisherSuggestionField,
					Logic.QueryPublisherSuggestions,
					nameof(Publisher.Name),
					nameof(Logic.Book.Publishers),
					false, PublisherValidationLabel, OnPublisherSelected);
		}

		private void OnAuthorSelected(object sender, TextInsertedEventArgs e) {
			e.SelectedValue = Logic.QueryAuthor(e.Text);
			if (e.SelectedValue == null) {
				if (PromptNewItem(e.Text, Properties.Resources.AddAuthor, () => Logic.AddAuthor(new Author() { Name = e.Text }))) {
					e.SelectedValue = Logic.QueryAuthor(e.Text);
				}
			}
		}

		private void OnClassificationSelected(object sender, TextInsertedEventArgs e) {
			e.SelectedValue = Logic.QueryClassification(e.Text);
			if (e.SelectedValue == null) {
				if (PromptNewItem(e.Text, Properties.Resources.AddClassification, () => Logic.AddClassification(new Classification() { Name = e.Text }))) {
					e.SelectedValue = Logic.QueryClassification(e.Text);
				}
			}
		}

		private void OnTitleSelected(object sender, TextInsertedEventArgs e) {
			e.SelectedValue = Logic.QueryTitle(e.Text);
			if (e.SelectedValue == null) {
				if (PromptNewItem(e.Text, Properties.Resources.AddTitle, () => Logic.AddTitle(new Title() { Name = e.Text }))) {
					e.SelectedValue = Logic.QueryTitle(e.Text);
				}
			}
		}

		private void OnPublisherSelected(object sender, TextInsertedEventArgs e) {
			e.SelectedValue = Logic.QueryPublisher(e.Text);
			if (e.SelectedValue == null) {
				if (PromptNewItem(e.Text, Properties.Resources.AddPublisher, () => Logic.AddPublisher(new Publisher() { Name = e.Text }))) {
					e.SelectedValue = Logic.QueryPublisher(e.Text);
				}
			}
		}

		private void InitializeSuggestionField(SuggestionField SuggestionField, Func<string, IList> itemsSourceFunc, string displayMemberPath, string bookPath, bool mandatory, Label validationLabel, TextInsertedHandler textSelected) {
			SuggestionField.LoadFunc = itemsSourceFunc;
			string selectedValuesPath = string.Join(".", nameof(Logic.Book), bookPath);
			BindingManager bindingManager = new BindingManager(SuggestionField, SuggestionField.SelectedValuesProperty, Logic, selectedValuesPath) {
				ValidationResultElement = validationLabel
			};
			SuggestionField.DisplayMemberPath = displayMemberPath;
			SuggestionField.MinItemCount = mandatory ? 1 : 0;
			SuggestionField.RemoveHandler(SuggestionFieldElement.TextInsertedEvent, textSelected);
			SuggestionField.AddHandler(SuggestionFieldElement.TextInsertedEvent, textSelected);
			bindingManager.ValidationRules.Add(new NullListValueValidationRule());
			AddBindingManager(bindingManager);
			SuggestionField.AddHandler(SuggestionFieldElement.ValidateEvent, new RoutedEventHandler((sender, e) => {
				bindingManager.Validate();
			}));
			SuggestionField.AddHandler(SuggestionField.FullValidateEvent, new RoutedEventHandler((sender, e) => {
				bindingManager.Validate();
			}));
		}

		private bool PromptNewItem(string text, string header, Action action) {
			MessageBoxResult result = MessageBox.Show(string.Format(Properties.Resources.ListItemNotExist, text), header, MessageBoxButton.YesNo, MessageBoxImage.Question);
			if (result == MessageBoxResult.Yes) {
				action();
			}
			return result == MessageBoxResult.Yes;
		}

		private BindingManager CreateBookStringBinding(FrameworkElement element, string property) {
			return new BindingManager(element, TextBox.TextProperty, Logic, string.Join(".", nameof(Logic.Book), property)) {
				Converter = new AllToStringConverter()
			};
		}

		private void BookEditorView_Loaded(object sender, RoutedEventArgs e) {
			Load();
		}

		private void Load() {
			Logic.OnLoad(ExternalBookId);
			if (!IsInitializatedOnce) {
				BindAndMask();
				IsInitializatedOnce = true;
			}
			if (Logic.Book.BookId != 0) {
				AddButton.Content = Properties.Resources.Change;
				HeaderLabel.Content = Properties.Resources.ChangeBook;
			} else {
				AddButton.Content = Properties.Resources.Add;
				HeaderLabel.Content = Properties.Resources.AddBook;
			}
		}
	}
}
