﻿using Skeinan.Bookcase.Utils;
using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Skeinan.Bookcase.Views.Utils {
	public class SearchService {
		private DataGrid mMainGrid;
		private DataGrid mSearchGrid;

		public SearchService(DataGrid mainGrid, DataGrid searchGrid) {
			mMainGrid = mainGrid;
			mSearchGrid = searchGrid;
		}

		public void AddColumn(string header, string path, string searchPath, Type valueType = null) {
			DataGridTextColumn column;
			column = new DataGridTextColumn {
				Header = header
			};
			Binding gridBinding = new Binding(path) {
				TargetNullValue = Properties.Resources.NotApplicable
			};
			if (typeof(DateTime).IsAssignableFrom(valueType)) {
				gridBinding.StringFormat = Util.LocalizedDateTimeFormat;
			} else if (typeof(bool).IsAssignableFrom(valueType)) {
				gridBinding.Converter = new BoolToLocalizedStringConverter();
			}
			column.Binding = gridBinding;
			mMainGrid.Columns.Add(column);

			Binding searchBinding = new Binding(searchPath);
			searchBinding.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
			DataGridTextColumn searchColumn = new DataGridTextColumn {
				Header = header,
			};
			searchColumn.Binding = searchBinding;

			Binding widthBinding = new Binding {
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				Mode = BindingMode.TwoWay,
				Source = searchColumn,
				Path = new PropertyPath(DataGridColumn.WidthProperty)
			};
			mSearchGrid.Columns.Add(searchColumn);
			BindingOperations.SetBinding(column, DataGridTextColumn.WidthProperty, widthBinding);
		}
	}
}
