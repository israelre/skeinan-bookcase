﻿using SkeinanWpf;
using SkeinanWpf.Masks;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.DependencyFactories;
using System.Windows.Controls;
using Skeinan.Bookcase.Views.Validators;

namespace Skeinan.Bookcase.Views {
	public partial class StudentEditorView : TabContent {
		public long ExternalStudentId { get; set; }

		public StudentService Logic { get; }
		public Session ActiveSession { get; }
		public StudentEditorFactory StudentEditorFactory { get; }

		public StudentEditorView(Session session, StudentEditorFactory studentEditorFactory, StudentService logic) {
			ActiveSession = session;
			Logic = logic;
			StudentEditorFactory = studentEditorFactory;

			InitializeComponent();
			PostInitializeComponent();

			Loaded += StudentEditorView_Loaded;
			AddButton.Click += AddButton_Click;
		}

		private void StudentEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad(ExternalStudentId);
			if (Logic.Student.StudentId != 0) {
				AddButton.Content = Properties.Resources.Change;
				HeaderLabel.Content = Properties.Resources.ChangeStudent;
			} else {
				AddButton.Content = Properties.Resources.Add;
				HeaderLabel.Content = Properties.Resources.AddStudent;
			}

			BindAll();

			new NumericMask(YearTextBox).Attach();
			new NumericMask(GradeTextBox).Attach();
			new NumericMask(NumberTextBox).Attach();
		}

		private void BindAll() {
			BindingManager codeBinding = BindToStudent(CodeTextBox, TextBox.TextProperty, nameof(Logic.Student.Code));
			codeBinding.ValidationResultElement = CodeValidationLabel;
			codeBinding.ValidationRules.Add(new MandatoryValidationRule());
			codeBinding.ValidationRules.Add(new StudentCodeValidationRule(Logic));

			BindingManager nameBinding = BindToStudent(NameTextBox, TextBox.TextProperty, nameof(Logic.Student.Name));
			nameBinding.ValidationResultElement = NameValidationLabel;
			nameBinding.ValidationRules.Add(new MandatoryValidationRule());

			BindToStudent(RegistryTextBox, TextBox.TextProperty, nameof(Logic.Student.Registry));
			BindToStudent(TransferredCheckBox, CheckBox.IsCheckedProperty, nameof(Logic.Student.Transferred));
			BindToStudent(YearTextBox, TextBox.TextProperty, nameof(Logic.Student.Year));
			BindToStudent(GradeTextBox, TextBox.TextProperty, nameof(Logic.Student.Grade));
			BindToStudent(ClassroomTextBox, TextBox.TextProperty, nameof(Logic.Student.Classroom));
			BindToStudent(NumberTextBox, TextBox.TextProperty, nameof(Logic.Student.Number));
			BindToStudent(BirthDatePicker, DatePicker.SelectedDateProperty, nameof(Logic.Student.BirthDate));
		}

		private BindingManager BindToStudent(FrameworkElement element, DependencyProperty dp, string path) {
			return AddBindingManager(element, dp, Logic, string.Join(".", nameof(Logic.Student), path));
		}

		private void AddButton_Click(object sender, RoutedEventArgs e) {
			bool exists = Logic.Student.StudentId != 0;

			string messageBoxCaption = exists ? Properties.Resources.ChangeStudent : Properties.Resources.AddStudent;
			string validationMessage = exists ? Properties.Resources.ChangeInvalid : Properties.Resources.AddInvalid;

			if (!Validate(validationMessage, messageBoxCaption)) {
				return;
			}

			if (exists) {
				MessageBoxResult result = MessageBox.Show(Properties.Resources.ChangeStudentConfirmation, messageBoxCaption, MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
				if (result != MessageBoxResult.Yes) {
					return;
				}
			}

			string message = exists ? Properties.Resources.ChangeStudentSuccess : Properties.Resources.AddStudentSuccess;
			if (Save(message, messageBoxCaption, Logic.Save)) {
				if (exists) OnClose(); else OnRefresh(StudentEditorFactory.Create(ActiveSession));
			}
		}
	}
}
