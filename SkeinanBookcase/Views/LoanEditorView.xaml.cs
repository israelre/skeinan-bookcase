﻿using SkeinanWpf;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.DependencyFactories;
using System.Windows.Data;

namespace Skeinan.Bookcase.Views {
	public partial class LoanEditorView : TabContent {
		public LoanService Logic { get; }
		public LoanEditorFactory LoanEditorFactory { get; }
		public Session ActiveSession { get; }

		public long ExternalStudentId { get; set; }
		public long ExternalBookId { get; set; }

		protected MandatoryValidationRule ReturnDateValidationRule { get; set; } = new MandatoryValidationRule();

		private bool mIsStudentCode = true;
		public bool IsStudentCode {
			get { return mIsStudentCode; }
			set { mIsStudentCode = value; NotifyPropertyChanged(); }
		}

		private bool mIsStudentName;
		public bool IsStudentName {
			get { return mIsStudentName; }
			set { mIsStudentName = value; NotifyPropertyChanged(); }
		}

		private bool HasLoan() {
			return Logic.Loan != null && Logic.Loan.LoanId != 0;
		}

		private bool mIsLoanEnabled;
		public bool IsLoanEnabled {
			get { return mIsLoanEnabled; }
			set { mIsLoanEnabled = value; NotifyPropertyChanged(); }
		}

		private bool mIsReturnEnabled;
		public bool IsReturnEnabled {
			get { return mIsReturnEnabled; }
			set { mIsReturnEnabled = value; NotifyPropertyChanged(); }
		}

		private void RefreshEnabledStatus() {
			IsLoanEnabled = Logic.SelectedBook == null || (!HasLoan() && !Logic.ChangePending);
			IsReturnEnabled = Logic.SelectedBook != null && (HasLoan() && !Logic.ChangePending);
		}

		private void LoanChanged() {
			Dispatcher.Invoke(() => {
				RefreshEnabledStatus();
				LoanButton.Content = HasLoan() ? Properties.Resources.Return : Properties.Resources.Loan;
				ReturnDatePicker.GetBindingExpression(DatePicker.SelectedDateProperty).UpdateTarget();
				LoanDatePicker.GetBindingExpression(DatePicker.SelectedDateProperty).UpdateTarget();
			});
		}

		private void ChangePendingChanged() {
			Dispatcher.Invoke(() => {
				RefreshEnabledStatus();
			});
		}

		public LoanEditorView(Session session, LoanEditorFactory loanEditorFactory, LoanService logic) {
			ActiveSession = session;
			Logic = logic;
			LoanEditorFactory = loanEditorFactory;

			InitializeComponent();
			PostInitializeComponent();

			Loaded += LoanEditorView_Loaded;
			LoanButton.Click += LoanButton_Click;

			AddListener(nameof(IsStudentCode), OnStudentModeChanged);
			AddListener(nameof(IsStudentName), OnStudentModeChanged);
			Logic.AddListener(nameof(Logic.Loan), LoanChanged);
			Logic.AddListener(nameof(Logic.SelectedBook), LoanChanged);
			Logic.AddListener(nameof(Logic.ChangePending), ChangePendingChanged);

			BookComboBox.ItemsSource = Logic.Books;
			BookComboBox.DisplayMemberPath = nameof(MinorBook.Code);
			WpfUtil.BlockInvalidSelection(BookComboBox);
			BindingManager bookComboBoxBinding = new BindingManager(BookComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.MinorSelectedBook)) {
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				ValidationResultElement = BookValidationLabel
			};
			bookComboBoxBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(bookComboBoxBinding);

			StudentComboBox.ItemsSource = Logic.Students;
			StudentComboBox.DisplayMemberPath = nameof(Student.Code);
			WpfUtil.BlockInvalidSelection(StudentComboBox);
			BindingManager studentComboBoxBinding = new BindingManager(StudentComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.SelectedStudent)) {
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				ValidationResultElement = StudentValidationLabel
			};
			studentComboBoxBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(studentComboBoxBinding);

			string loanDatePath = string.Join(".", nameof(Logic.Loan), nameof(Logic.Loan.LoanDate));
			BindingManager loanDateBinding = new BindingManager(LoanDatePicker, DatePicker.SelectedDateProperty, Logic, loanDatePath) {
				ValidationResultElement = LoanDateValidationLabel
			};
			loanDateBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(loanDateBinding);

			string returnDatePath = string.Join(".", nameof(Logic.Loan), nameof(Logic.Loan.ReturnDate));
			BindingManager returnDateBinding = new BindingManager(ReturnDatePicker, DatePicker.SelectedDateProperty, Logic, returnDatePath) {
				ValidationResultElement = ReturnDateValidationLabel
			};
			returnDateBinding.ValidationRules.Add(ReturnDateValidationRule);
			AddBindingManager(returnDateBinding);

			AddBindingManager(TitleLabel, Label.ContentProperty, Logic, nameof(Logic.SelectedBookTitle));
			string studentNamePath = string.Join(".", nameof(Logic.SelectedStudent), nameof(Logic.SelectedStudent.Name));
			AddBindingManager(NameLabel, Label.ContentProperty, Logic, studentNamePath);

			BindingManager codeBindingManager = new BindingManager(CodeRadioButton, RadioButton.IsCheckedProperty, this, nameof(IsStudentCode));
			codeBindingManager.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			AddBindingManager(codeBindingManager);
			BindingManager nameBindingManager = new BindingManager(NameRadioButton, RadioButton.IsCheckedProperty, this, nameof(IsStudentName));
			nameBindingManager.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
			AddBindingManager(nameBindingManager);

			AddBindingManager(StudentComboBox, ComboBox.IsEnabledProperty, this, nameof(IsLoanEnabled));
			AddBindingManager(LoanDatePicker, DatePicker.IsEnabledProperty, this, nameof(IsLoanEnabled));
			AddBindingManager(ReturnDatePicker, DatePicker.IsEnabledProperty, this, nameof(IsReturnEnabled));

			AddListener(nameof(IsReturnEnabled), OnReturnEnabledChanged);
		}

		private void LoanEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad(ExternalBookId, ExternalStudentId);
		}

		private void OnReturnEnabledChanged() {
			ReturnDateValidationRule.Enabled = IsReturnEnabled;
		}

		private void LoanButton_Click(object sender, RoutedEventArgs e) {
			if (!Validate(Properties.Resources.AddInvalid, Properties.Resources.AddLoan)) {
				return;
			}

			string rawMessage = Logic.Loan.ReturnDate != null ? Properties.Resources.AddReturnSuccess : Properties.Resources.AddLoanSuccess;
			string message = string.Format(CultureInfo.CurrentUICulture, rawMessage, Logic.SelectedBookTitle, Logic.SelectedStudent.Name);
			if (Save(message, Properties.Resources.AddLoan, Logic.Save)) {
				LoanEditorView content = LoanEditorFactory.Create(ActiveSession);
				content.ExternalStudentId = Logic.SelectedStudent.StudentId;
				Logic.OnLoad(ExternalBookId, ExternalStudentId);
			}
		}

		private void OnStudentModeChanged() {
			string path = IsStudentName ? nameof(Logic.SelectedStudent.Name) : nameof(Logic.SelectedStudent.Code);
			StudentComboBox.DisplayMemberPath = path;
		}
	}
}
