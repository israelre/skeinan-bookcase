﻿using System.Windows;
using System;
using Skeinan.Bookcase.DependencyFactories;
using SkeinanWpf.Tabs;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views {
    public partial class MainWindowView : Window {
		public Session ActiveSession { get; }

		public BookEditorFactory BookEditorFactory { get; }
		public StudentEditorFactory StudentEditorFactory { get; }
		public LoanEditorFactory LoanEditorFactory { get; }
		public AuthorEditorFactory AuthorEditorFactory { get; }
		public ClassificationEditorFactory ClassificationEditorFactory { get; }
		public PublisherEditorFactory PublisherEditorFactory { get; }
		public TitleEditorFactory TitleEditorFactory { get; }
		public BookSearchFactory BookSearchFactory { get; }
		public StudentSearchFactory StudentSearchFactory { get; }
		public LoanSearchFactory LoanSearchFactory { get; }

		public MainWindowView(Session session,
				BookEditorFactory bookEditorFactory,
				StudentEditorFactory studentEditorFactory,
				LoanEditorFactory loanEditorFactory,
				AuthorEditorFactory authorEditorFactory,
				ClassificationEditorFactory classificationEditorFactory,
				PublisherEditorFactory publisherEditorFactory,
				TitleEditorFactory titleEditorFactory,
				BookSearchFactory bookSearchFactory,
				StudentSearchFactory studentSearchFactory,
				LoanSearchFactory loanSearchFactory) {
			ActiveSession = session;
			BookEditorFactory = bookEditorFactory;
			StudentEditorFactory = studentEditorFactory;
			LoanEditorFactory = loanEditorFactory;
			AuthorEditorFactory = authorEditorFactory;
			ClassificationEditorFactory = classificationEditorFactory;
			PublisherEditorFactory = publisherEditorFactory;
			TitleEditorFactory = titleEditorFactory;
			BookSearchFactory = bookSearchFactory;
			StudentSearchFactory = studentSearchFactory;
			LoanSearchFactory = loanSearchFactory;

			WindowStartupLocation = WindowStartupLocation.CenterScreen;
			InitializeComponent();
			Loaded += Window_Loaded;
			MainStatusBar.Items.Add(Properties.Resources.ApplicationCopyright);
		}

		public void OpenAddBook(long bookID = 0) {
			BookEditorView content = BookEditorFactory.Create(ActiveSession);
			content.ExternalBookId = bookID;
			if (bookID != 0) {
				Open(Properties.Resources.ChangeBook, content, true);
			} else {
				Open(Properties.Resources.AddBook, content);
			}
		}

		public void OpenAddStudent(long studentId = 0) {
			StudentEditorView content = StudentEditorFactory.Create(ActiveSession);
			content.ExternalStudentId = studentId;
			if (studentId != 0) {
				Open(Properties.Resources.ChangeStudent, content, true);
			} else {
				Open(Properties.Resources.AddStudent, content);
			}
		}

		public void OpenAddLoanForBook(long bookID = 0) {
			LoanEditorView content = LoanEditorFactory.Create(ActiveSession);
			content.ExternalBookId = bookID;
			Open(Properties.Resources.AddLoan, content);
		}

		public void OpenAddLoanForStudent(long studentId = 0) {
			LoanEditorView content = LoanEditorFactory.Create(ActiveSession);
			content.ExternalStudentId = studentId;
			Open(Properties.Resources.AddLoan, content);
		}

		private void Open(string tabTitle, TabContent content, bool overwrite = false) {
			TabData item = new TabData {
				Title = tabTitle,
				Content = content
			};
			MainTabbedControl.Add(item, overwrite);
		}

		private void AttachTab(MenuItem menuItem, string tabTitle, Func<TabContent> createTab) {
			menuItem.Click += (sender, e) => {
				Open(tabTitle, createTab());
			};
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			AttachTab(AddBookMenuItem, Properties.Resources.AddBook, () => BookEditorFactory.Create(ActiveSession));
			AttachTab(AddLoanMenuItem, Properties.Resources.AddLoan, () => LoanEditorFactory.Create(ActiveSession));
			AttachTab(AddStudentMenuItem, Properties.Resources.AddStudent, () => StudentEditorFactory.Create(ActiveSession));
			AttachTab(ChangeAuthorMenuItem, Properties.Resources.ChangeAuthor, () => AuthorEditorFactory.Create(ActiveSession));
			AttachTab(ChangeClassificationMenuItem, Properties.Resources.ChangeClassification, () => ClassificationEditorFactory.Create(ActiveSession));
			AttachTab(ChangePublisherMenuItem, Properties.Resources.ChangePublisher, () => PublisherEditorFactory.Create(ActiveSession));
			AttachTab(ChangeTitleMenuItem, Properties.Resources.ChangeTitle, () => TitleEditorFactory.Create(ActiveSession));
			AttachTab(SearchBookMenuItem, Properties.Resources.SearchBook, () => BookSearchFactory.Create(ActiveSession));
			AttachTab(SearchStudentMenuItem, Properties.Resources.SearchStudent, () => StudentSearchFactory.Create(ActiveSession));
			AttachTab(SearchLoanMenuItem, Properties.Resources.SearchLoan, () => LoanSearchFactory.Create(ActiveSession));
		}
	}
}
