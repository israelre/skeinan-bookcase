﻿using SkeinanWpf;
using SkeinanWpf.Tabs;
using SkeinanWpf.Validators;
using System.Windows;
using Skeinan.Bookcase.Services;
using System.Windows.Controls;

namespace Skeinan.Bookcase.Views {
	public partial class AuthorEditorView : TabContent {
		public AuthorService Logic { get; }

		public AuthorEditorView(AuthorService logic) {
			Logic = logic;
			InitializeComponent();
			PostInitializeComponent();
			Loaded += AuthorEditorView_Loaded;
			ChangeButton.Click += ChangeButton_Click;
		}

		private void AuthorEditorView_Loaded(object sender, RoutedEventArgs e) {
			Logic.OnLoad();
			BindAndMask();
		}

		private void BindAndMask() {
			OldNameComboBox.ItemsSource = Logic.Authors;
			OldNameComboBox.DisplayMemberPath = nameof(Logic.Author.Name);
			BindingManager oldNameBinding = new BindingManager(OldNameComboBox, ComboBox.SelectedValueProperty, Logic, nameof(Logic.Author)) {
				ValidationResultElement = OldNameValidationLabel,
			};
			oldNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(oldNameBinding);

			BindingManager newNameBinding = new BindingManager(NewNameTextBox, TextBox.TextProperty, Logic, nameof(Logic.NewName)) {
				ValidationResultElement = NewNameValidationLabel
			};
			newNameBinding.ValidationRules.Add(new MandatoryValidationRule());
			AddBindingManager(newNameBinding);
		}

		private void ChangeButton_Click(object sender, RoutedEventArgs e) {
			if (Validate(Properties.Resources.ChangeInvalid, Properties.Resources.ChangeAuthor)
					&& Save(Properties.Resources.ChangeSuccess, Properties.Resources.ChangeAuthor, Logic.Save)) {
				OnClose();
			}
		}
	}
}
