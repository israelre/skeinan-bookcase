﻿using Skeinan.Bookcase.Utils;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Skeinan.Bookcase.Views {
	public class BoolToLocalizedStringConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null) value = false;
			return Util.BoolToLocalizedString((bool)value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
