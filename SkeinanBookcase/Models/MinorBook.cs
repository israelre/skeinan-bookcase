﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skeinan.Bookcase.Models {
	public class MinorBook {
		public long BookId { get; set; }
		public string Code { get; set; }
		public string Title { get; set; }
	}
}
