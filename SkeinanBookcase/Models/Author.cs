﻿namespace Skeinan.Bookcase.Models {
	public class Author : Model {
		private long mAuthorId;
		public long AuthorId {
			get { return mAuthorId; }
			set { mAuthorId = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}
	}
}

