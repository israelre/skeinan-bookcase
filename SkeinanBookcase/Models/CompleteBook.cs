﻿using System.ComponentModel;

namespace Skeinan.Bookcase.Models {
	public class CompleteBook : Book {
		public BindingList<Title> Titles { get; set; } = new BindingList<Title>();
		public BindingList<Author> Authors { get; set; } = new BindingList<Author>();
		public BindingList<Classification> Classifications { get; set; } = new BindingList<Classification>();
		public BindingList<Publisher> Publishers { get; set; } = new BindingList<Publisher>();
	}
}
