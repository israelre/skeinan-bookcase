﻿using System;

namespace Skeinan.Bookcase.Models {
	public class Book : Model {
		private long mBookId;
		public long BookId {
			get { return mBookId; }
			set { mBookId = value; NotifyPropertyChanged(); }
		}

		private string mCode;
		public string Code {
			get { return mCode; }
			set { mCode = value; NotifyPropertyChanged(); }
		}

		private string mIsbn;
		public string Isbn {
			get { return mIsbn; }
			set { mIsbn = value; NotifyPropertyChanged(); }
		}

		private int? mYear;
		public int? Year {
			get { return mYear; }
			set { mYear = value; NotifyPropertyChanged(); }
		}

		private int? mEdition;
		public int? Edition {
			get { return mEdition; }
			set { mEdition = value; NotifyPropertyChanged(); }
		}

		private string mSource;
		public string Source {
			get { return mSource; }
			set { mSource = value; NotifyPropertyChanged(); }
		}

		private DateTime? mAddDate;
		public DateTime? AddDate {
			get { return mAddDate; }
			set { mAddDate = value; NotifyPropertyChanged(); }
		}
	}
}
