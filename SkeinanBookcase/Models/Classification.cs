﻿namespace Skeinan.Bookcase.Models {
	public class Classification : Model {
		private long mClassificationId;
		public long ClassificationId {
			get { return mClassificationId; }
			set { mClassificationId = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}
	}
}
