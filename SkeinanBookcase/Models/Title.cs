﻿namespace Skeinan.Bookcase.Models {
	public class Title : Model {
		private long mTitleId;
		public long TitleId {
			get { return mTitleId; }
			set { mTitleId = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}
	}
}
