﻿using System;

namespace Skeinan.Bookcase.Models {
	public class Student : Model {
		private long mStudentId;
		public long StudentId {
			get { return mStudentId; }
			set { mStudentId = value; NotifyPropertyChanged(); }
		}

		private string mCode;
		public string Code {
			get { return mCode; }
			set { mCode = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}

		private string mRegistry;
		public string Registry {
			get { return mRegistry; }
			set { mRegistry = value; NotifyPropertyChanged(); }
		}

		private bool mTransferred;
		public bool Transferred {
			get { return mTransferred; }
			set { mTransferred = value; NotifyPropertyChanged(); }
		}

		private int? mYear;
		public int? Year {
			get { return mYear; }
			set { mYear = value; NotifyPropertyChanged(); }
		}

		private int? mGrade;
		public int? Grade {
			get { return mGrade; }
			set { mGrade = value; NotifyPropertyChanged(); }
		}

		private string mClassroom;
		public string Classroom {
			get { return mClassroom; }
			set { mClassroom = value; NotifyPropertyChanged(); }
		}

		private int? mNumber;
		public int? Number {
			get { return mNumber; }
			set { mNumber = value; NotifyPropertyChanged(); }
		}

		private DateTime? mBirthDate;
		public DateTime? BirthDate {
			get { return mBirthDate; }
			set { mBirthDate = value; NotifyPropertyChanged(); }
		}
	}
}
