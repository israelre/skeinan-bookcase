﻿using System;

namespace Skeinan.Bookcase.Models {
	public class Loan : Model {
		private long mLoanId;
		public long LoanId {
			get { return mLoanId; }
			set { mLoanId = value; NotifyPropertyChanged(); }
		}

		private long mBookId;
		public long BookId {
			get { return mBookId; }
			set { mBookId = value; NotifyPropertyChanged(); }
		}

		private long mStudentId;
		public long StudentId {
			get { return mStudentId; }
			set { mStudentId = value; NotifyPropertyChanged(); }
		}

		private DateTime mLoanDate;
		public DateTime LoanDate {
			get { return mLoanDate; }
			set { mLoanDate = value; NotifyPropertyChanged(); }
		}

		private DateTime? mReturnDate;
		public DateTime? ReturnDate {
			get { return mReturnDate; }
			set { mReturnDate = value; NotifyPropertyChanged(); }
		}
	}
}
