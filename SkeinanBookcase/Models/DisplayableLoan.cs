﻿namespace Skeinan.Bookcase.Models {
	public class DisplayableLoan : Loan {
		public string BookTitle { get; set; }
		public string StudentName { get; set; }
	}
}
