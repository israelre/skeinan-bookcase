﻿using System;

namespace Skeinan.Bookcase.Models {
	public class DisplayableBook : Book {
		public string Title { get; set; }
		public string Author { get; set; }
		public string Classification { get; set; }
		public string Publisher { get; set; }
		public DateTime? LoanDate { get; set; }
	}
}
