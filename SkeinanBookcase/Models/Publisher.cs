﻿namespace Skeinan.Bookcase.Models {
	public class Publisher : Model {
		private long mPublisherId;
		public long PublisherId {
			get { return mPublisherId; }
			set { mPublisherId = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}
	}
}
