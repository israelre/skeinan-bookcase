﻿namespace Skeinan.Bookcase.Models {
	public class User : Model {
		private long mUserId;
		public long AuthorId {
			get { return mUserId; }
			set { mUserId = value; NotifyPropertyChanged(); }
		}

		private string mName;
		public string Name {
			get { return mName; }
			set { mName = value; NotifyPropertyChanged(); }
		}

		private string mHashedPassword;
		public string HashedPassword
		{
			get { return mHashedPassword; }
			set { mHashedPassword = value; NotifyPropertyChanged(); }
		}

		private string mSalt;
		public string Salt
		{
			get { return mSalt; }
			set { mSalt = value; NotifyPropertyChanged(); }
		}
	}
}

