﻿using System;

namespace Skeinan.Bookcase.Models {
	public class DisplayableStudent : Student {
		public string BookTitle { get; set; }
	}
}
