﻿using SkeinanWpf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Skeinan.Bookcase.Models {
	public class Model : INotifyPropertyChanged, ICloneable {
		public event PropertyChangedEventHandler PropertyChanged;
		public PropertyChangeManager PropertyChangeManager { get; }

		public void NotifyPropertyChanged([CallerMemberName] String propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void AddListener(string propertyName, Action action) {
			PropertyChangeManager.AddListener(propertyName, action);
		}

		public object Clone() {
			return MemberwiseClone();
		}

		public Model() {
			PropertyChangeManager = new PropertyChangeManager(this);
		}
	}
}
