﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class StudentEditorFactory {
		private PathService PathService { get; }

		public StudentEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public StudentEditorView Create(Session session) {
			StudentEditorView view = new StudentEditorView(session, this, new StudentService(session, PathService));
			return view;
		}
	}
}
