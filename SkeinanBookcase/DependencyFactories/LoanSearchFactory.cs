﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class LoanSearchFactory {
		private PathService PathService { get; }

		public LoanSearchFactory(PathService pathService) {
			PathService = pathService;
		}

		public LoanSearchView Create(Session session) {
			LoanSearchView view = new LoanSearchView(new LoanSearchService(session, PathService));
			return view;
		}
	}
}
