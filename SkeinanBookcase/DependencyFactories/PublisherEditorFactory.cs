﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class PublisherEditorFactory {
		private PathService PathService { get; }

		public PublisherEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public PublisherEditorView Create(Session session) {
			PublisherEditorView view = new PublisherEditorView(new PublisherService(session, PathService));
			return view;
		}
	}
}
