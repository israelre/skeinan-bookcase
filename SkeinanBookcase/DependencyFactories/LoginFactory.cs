﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class LoginFactory {
		public PathService PathService { get; }
		public MainWindowFactory MainWindowFactory { get; set; }

		public LoginFactory(PathService pathService, MainWindowFactory mainWindowFactory) {
			PathService = pathService;
			MainWindowFactory = mainWindowFactory;
		}

		public LoginView Create() {
			LoginView view = new LoginView(new LoginService(PathService), MainWindowFactory);
			return view;
		}
	}
}
