﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class TitleEditorFactory {
		private PathService PathService { get; }

		public TitleEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public TitleEditorView Create(Session session) {
			TitleEditorView view = new TitleEditorView(new TitleService(session, PathService));
			return view;
		}
	}
}
