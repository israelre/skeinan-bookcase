﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class AuthorEditorFactory {
		private PathService PathService { get; }

		public AuthorEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public AuthorEditorView Create(Session session) {
			AuthorEditorView view = new AuthorEditorView(new AuthorService(session, PathService));
			return view;
		}
	}
}
