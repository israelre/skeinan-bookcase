﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class BookSearchFactory {
		private PathService PathService { get; }

		public BookSearchFactory(PathService pathService) {
			PathService = pathService;
		}

		public BookSearchView Create(Session session) {
			BookSearchView view = new BookSearchView(new BookSearchService(session, PathService));
			return view;
		}
	}
}
