﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class BookEditorFactory {
		private PathService PathService { get; }

		public BookEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public BookEditorView Create(Session session) {
			BookEditorView view = new BookEditorView(new BookService(session, PathService));
			return view;
		}
	}
}
