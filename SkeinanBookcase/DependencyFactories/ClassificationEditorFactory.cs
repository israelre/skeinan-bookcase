﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class ClassificationEditorFactory {
		private PathService PathService { get; }

		public ClassificationEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public ClassificationEditorView Create(Session session) {
			ClassificationEditorView view = new ClassificationEditorView(new ClassificationService(session, PathService));
			return view;
		}
	}
}
