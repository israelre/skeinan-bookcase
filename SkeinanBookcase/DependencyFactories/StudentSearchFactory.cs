﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Services.Searchers;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class StudentSearchFactory {
		private PathService PathService { get; }

		public StudentSearchFactory(PathService pathService) {
			PathService = pathService;
		}

		public StudentSearchView Create(Session session) {
			StudentSearchView view = new StudentSearchView(new StudentSearchService(session, PathService));
			return view;
		}
	}
}
