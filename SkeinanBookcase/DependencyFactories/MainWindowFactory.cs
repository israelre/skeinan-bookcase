﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;
using System;

namespace Skeinan.Bookcase.DependencyFactories {
	public class MainWindowFactory {
		private Func<Session, MainWindowView> mCreationFunc;

		public MainWindowFactory(
				BookEditorFactory bookEditorFactory,
				StudentEditorFactory studentEditorFactory,
				LoanEditorFactory loanEditorFactory,
				AuthorEditorFactory authorEditorFactory,
				ClassificationEditorFactory classificationEditorFactory,
				PublisherEditorFactory publisherEditorFactory,
				TitleEditorFactory titleEditorFactory,
				BookSearchFactory bookSearchFactory,
				StudentSearchFactory studentSearchFactory,
				LoanSearchFactory loanSearchFactory) {
			mCreationFunc = (Session session) => new MainWindowView(
					session,
					bookEditorFactory,
					studentEditorFactory,
					loanEditorFactory,
					authorEditorFactory,
					classificationEditorFactory,
					publisherEditorFactory,
					titleEditorFactory,
					bookSearchFactory,
					studentSearchFactory,
					loanSearchFactory);
		}

		public MainWindowView Create(Session session) {
			MainWindowView view = mCreationFunc(session);
			return view;
		}
	}
}
