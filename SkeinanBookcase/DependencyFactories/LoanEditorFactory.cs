﻿using Skeinan.Bookcase.Services;
using Skeinan.Bookcase.Views;

namespace Skeinan.Bookcase.DependencyFactories {
	public class LoanEditorFactory {
		private PathService PathService { get; }

		public LoanEditorFactory(PathService pathService) {
			PathService = pathService;
		}

		public LoanEditorView Create(Session session) {
			LoanEditorView view = new LoanEditorView(session, this, new LoanService(session, PathService));
			return view;
		}
	}
}
