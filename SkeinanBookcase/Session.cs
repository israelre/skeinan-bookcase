﻿using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase {
	public class Session {
		public User User { get; set; }
	}
}
