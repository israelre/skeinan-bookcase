﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;

namespace Skeinan.Bookcase.Services {
	public class BookService : Service {
		private const int SUGGESTION_LIMIT = 4;

		public Session ActiveSession { get; }
		public PathService PathService { get; }

		private CompleteBook mBook;
		public CompleteBook Book {
			get { return mBook; }
			set { mBook = value; NotifyPropertyChanged(); }
		}

		public BindingList<Title> AllTitles { get; } = new BindingList<Title>();
		public BindingList<Author> AllAuthors { get; } = new BindingList<Author>();
		public BindingList<Classification> AllClassifications { get; } = new BindingList<Classification>();
		public BindingList<Publisher> AllPublishers { get; } = new BindingList<Publisher>();

		public BookService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public bool BookCodeExists(string code) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				return context.Books.Any(e => e.BookId != Book.BookId && e.Code == code);
			}
		}

		public void OnLoad(long externalBookId = 0) {
			if (Book == null) {
				if (externalBookId != 0) {
					using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
						BookEntity bookEntity = context.Books.Where(entity => entity.BookId == externalBookId).AsNoTracking().FirstOrDefault();
						Book = CompleteBookMapper.Instance.MapToModel(bookEntity);
					}
				} else {
					Book = new CompleteBook();
					Book.AddDate = DateTime.Now;
				}
			}
			RefreshTitles();
			RefreshAuthors();
			RefreshClassifications();
			RefreshPublishers();
		}

		private void RefreshTitles() {
			List<long?> selected = Book?.Titles?.Select(e => e?.TitleId).ToList();
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.EnableLog();
				IQueryable<TitleEntity> titles = context.Titles;
				titles = titles.AsNoTracking();
				titles = titles.OrderBy(e => e.Name);
				Util.RefreshCompleteList(AllTitles,
						titles,
						TitleMapper.Instance.MapToModel,
						Book?.Titles,
						e => e?.TitleId);
			}
		}

		private void RefreshAuthors() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				Util.RefreshCompleteList(AllAuthors,
						context.Authors.AsNoTracking().OrderBy(e => e.Name),
						AuthorMapper.Instance.MapToModel,
						Book?.Authors,
						e => e?.AuthorId);
			}
		}

		private void RefreshClassifications() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				Util.RefreshCompleteList(AllClassifications,
						context.Classifications.AsNoTracking().OrderBy(e => e.Name),
						ClassificationMapper.Instance.MapToModel,
						Book?.Classifications,
						e => e?.ClassificationId);
			}
		}

		private void RefreshPublishers() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				Util.RefreshCompleteList(AllPublishers,
						context.Publishers.AsNoTracking().OrderBy(e => e.Name),
						PublisherMapper.Instance.MapToModel,
						Book?.Publishers,
						e => e?.PublisherId);
			}
		}

		public void Save() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				if (Book.BookId != 0) {
					BookEntity existing = context.Books.Where(e => e.BookId == Book.BookId).First();
					CompleteBookMapper.Instance.MapToEntity(Book, existing);
				} else {
					context.Books.Add(CompleteBookMapper.Instance.MapToEntity(Book));
				}

				context.SaveChanges();
			}
		}

		public void Reset() {
			Book = new CompleteBook();
		}

		public void ResetIdentification() {
			Book.BookId = 0;
			Book.Code = null;
		}

		public Title QueryTitle(String name) {
			Title title;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				title = TitleMapper.Instance.MapToModel(context.Titles.Where(e => e.Name == name).FirstOrDefault());
			}
			return title;
		}

		public Author QueryAuthor(String name) {
			Author author;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				author = AuthorMapper.Instance.MapToModel(context.Authors.Where(e => e.Name == name).FirstOrDefault());
			}
			return author;
		}

		public Classification QueryClassification(String name) {
			Classification classification;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				classification = ClassificationMapper.Instance.MapToModel(context.Classifications.Where(e => e.Name == name).FirstOrDefault());
			}
			return classification;
		}

		public Publisher QueryPublisher(String name) {
			Publisher publisher;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				publisher = PublisherMapper.Instance.MapToModel(context.Publishers.Where(e => e.Name == name).FirstOrDefault());
			}
			return publisher;
		}

		public List<Title> QueryTitleSuggestions(String name) {
			name = name ?? "";
			List<Title> titles;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				titles = context.Titles.Where(e => e.Name.StartsWith(name)).OrderBy(e => e.Name).Take(SUGGESTION_LIMIT).ToList().ConvertAll(TitleMapper.Instance.MapToModel);
			}
			return titles;
		}

		public List<Author> QueryAuthorSuggestions(String name) {
			name = name ?? "";
			List<Author> authors;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				authors = context.Authors.Where(e => e.Name.StartsWith(name)).OrderBy(e => e.Name).Take(SUGGESTION_LIMIT).ToList().ConvertAll(AuthorMapper.Instance.MapToModel);
			}
			return authors;
		}

		public List<Classification> QueryClassificationSuggestions(String name) {
			name = name ?? "";
			List<Classification> classifications;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				classifications = context.Classifications.Where(e => e.Name.StartsWith(name)).OrderBy(e => e.Name).Take(SUGGESTION_LIMIT).ToList().ConvertAll(ClassificationMapper.Instance.MapToModel);
			}
			return classifications;
		}

		public List<Publisher> QueryPublisherSuggestions(String name) {
			name = name ?? "";
			List<Publisher> publishers;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				publishers = context.Publishers.Where(e => e.Name.StartsWith(name)).OrderBy(e => e.Name).Take(SUGGESTION_LIMIT).ToList().ConvertAll(PublisherMapper.Instance.MapToModel);
			}
			return publishers;
		}

		public void AddTitle(Title title) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.Titles.Add(TitleMapper.Instance.MapToEntity(title));
				context.SaveChanges();
			}
			RefreshTitles();
		}

		public void AddAuthor(Author author) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.Authors.Add(AuthorMapper.Instance.MapToEntity(author));
				context.SaveChanges();
			}
			RefreshAuthors();
		}

		public void AddClassification(Classification classification) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.Classifications.Add(ClassificationMapper.Instance.MapToEntity(classification));
				context.SaveChanges();
			}
			RefreshClassifications();
		}

		public void AddPublisher(Publisher publisher) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.Publishers.Add(PublisherMapper.Instance.MapToEntity(publisher));
				context.SaveChanges();
			}
			RefreshPublishers();
		}
	}
}
