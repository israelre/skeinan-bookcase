﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Mappers;

namespace Skeinan.Bookcase.Services {
	public class LoanService : Service {
		private BackgroundWorker BookChangeTask { get; set; }

		public Session ActiveSession { get; }
		public PathService PathService { get; }
		public BindingList<MinorBook> Books { get; } = new BindingList<MinorBook>();
		public BindingList<Student> Students { get; } = new BindingList<Student>();

		private Loan mLoan;
		public Loan Loan {
			get { return mLoan; }
			set { mLoan = value; NotifyPropertyChanged(); }
		}

		private MinorBook mMinorSelectedBook;
		public MinorBook MinorSelectedBook {
			get { return mMinorSelectedBook; }
			set { mMinorSelectedBook = value; NotifyPropertyChanged(); }
		}

		private Book mSelectedBook;
		public Book SelectedBook {
			get { return mSelectedBook; }
			set { mSelectedBook = value; NotifyPropertyChanged(); }
		}

		private Student mSelectedStudent;
		public Student SelectedStudent {
			get { return mSelectedStudent; }
			set { mSelectedStudent = value; NotifyPropertyChanged(); }
		}

		private string mSelectedBookTitle;
		public string SelectedBookTitle {
			get { return mSelectedBookTitle; }
			set { mSelectedBookTitle = value; NotifyPropertyChanged(); }
		}

		private bool mChangePending;
		public bool ChangePending {
			get { return mChangePending; }
			set { mChangePending = value; NotifyPropertyChanged(); }
		}

		public LoanService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
			AddListener(nameof(MinorSelectedBook), SelectedBookChanged);
		}

		protected void SelectedBookChanged() {
			BookChangeTask?.CancelAsync();
			BookChangeTask = new BackgroundWorker();
			BookChangeTask.WorkerSupportsCancellation = true;
			BookChangeTask.DoWork += BookChangeTask_DoWork;
			BookChangeTask.RunWorkerCompleted += BookChangeTask_RunWorkerCompleted;
			BookChangeTask.RunWorkerAsync(MinorSelectedBook);
		}

		private void BookChangeTask_DoWork(object sender, DoWorkEventArgs e) {
			MinorBook value = (MinorBook)e.Argument;
			Loan newLoan = null;
			long? newStudentId = null;
			string bookTitle = "";
			Book book = null;
			string title = null;
			if (value != null) {
				using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
					BookEntity bookEntity = context.Books.Where(b => b.BookId == value.BookId).FirstOrDefault();
					book = BookMapper.Instance.MapToModel(bookEntity);
					title = bookEntity.Titles.FirstOrDefault()?.Title.Name;
					if (e.Cancel) return;
					LoanEntity dbLoan = context.Loans.Where(entity => entity.Book.BookId == value.BookId && entity.ReturnDate == null).FirstOrDefault();
					newLoan = LoanMapper.Instance.MapToModel(dbLoan);
					newStudentId = dbLoan?.StudentId;
					if (e.Cancel) return;
					bookTitle = context.Books.Where(b => b.BookId == value.BookId).First().Titles.First().Title.Name;
				}
			}

			if (newLoan != null) {
				newLoan.ReturnDate = DateTime.Now;
			} else {
				newLoan = new Loan {
					LoanDate = DateTime.Now
				};
			}
			
			e.Result = new Tuple<long?, Loan, Book, string>(newStudentId, newLoan, book, title);
		}

		private void BookChangeTask_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
			Tuple<long?, Loan, Book, string> result = (Tuple<long?, Loan, Book, string>)e.Result;
			long? newStudentId = result.Item1;
			SelectedBook = result.Item3;
			SelectedBookTitle = result.Item4;
			if (newStudentId != null) SelectedStudent = Students.Where(s => s.StudentId == newStudentId).FirstOrDefault();
			Loan = result.Item2;
			ChangePending = false;
		}

		public void OnLoad(long currentBookId = 0, long currentStudentId = 0) {
			Books.RaiseListChangedEvents = false;
			Books.Clear();
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				List<BookEntity> books = context.Books.Include(b => b.Titles.Select(t => t.Title)).ToList();
				books.ForEach(b => Books.Add(MinorBookMapper.Instance.MapToModel(b)));
				if (currentBookId != 0) {
					MinorSelectedBook = Books.Where(entity => entity.BookId == currentBookId).FirstOrDefault();
				}
			}
			Books.RaiseListChangedEvents = true;
			Books.ResetBindings();

			Students.RaiseListChangedEvents = false;
			Students.Clear();
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				List<StudentEntity> students = context.Students.AsNoTracking().ToList();
				students.ForEach(s => Students.Add(StudentMapper.Instance.MapToModel(s)));
				if (currentStudentId != 0) {
					SelectedStudent = Students.Where(entity => entity.StudentId == currentStudentId).FirstOrDefault();
				}
			}
			Students.RaiseListChangedEvents = true;
			Students.ResetBindings();

			SelectedBookChanged();
		}

		public void Save() {
			Loan.BookId = MinorSelectedBook.BookId;
			Loan.StudentId = SelectedStudent.StudentId;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				LoanEntity existing = context.Loans.Where(entity => entity.Book.BookId == MinorSelectedBook.BookId && entity.ReturnDate == null).FirstOrDefault();
				if (existing != null) {
					LoanMapper.Instance.MapToEntity(Loan, existing);
				} else {
					context.Loans.Add(LoanMapper.Instance.MapToEntity(Loan));
				}
				context.SaveChanges();
			}
		}
	}
}
