﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System.ComponentModel;
using System.Linq;

namespace Skeinan.Bookcase.Services {
	public class AuthorService : Service {
		public Session ActiveSession { get; }
		public PathService PathService { get; }

		public BindingList<Author> Authors { get; } = new BindingList<Author>();

		private Author mAuthor;
		public Author Author {
			get { return mAuthor; }
			set { mAuthor = value; NotifyPropertyChanged(); }
		}

		private string mNewName;
		public string NewName {
			get { return mNewName; }
			set { mNewName = value; NotifyPropertyChanged(); }
		}

		public AuthorService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public void OnLoad() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				long? selectedId = Author?.AuthorId;
				Util.RefreshCompleteList(Authors,
						context.Authors.AsNoTracking().OrderBy(e => e.Name),
						AuthorMapper.Instance.MapToModel);
				Author = Authors.Where(e => e.AuthorId == selectedId).FirstOrDefault();
			}
		}

		public void Save() {
			Author.Name = NewName;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				AuthorEntity existing = context.Authors.Where(e => e.AuthorId == Author.AuthorId).FirstOrDefault();
				if (existing != null) {
					AuthorMapper.Instance.MapToEntity(Author, existing);
				} else {
					context.Authors.Add(AuthorMapper.Instance.MapToEntity(Author));
				}
				context.SaveChanges();
			}
		}
	}
}
