﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Skeinan.Bookcase.Services.Searchers {
	public class PageNumberToStringConverter : IValueConverter {

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			int pageNumber = (int)value;
			if (pageNumber == 0) {
				return Properties.Resources.NoResults;
			} else {
				return string.Format(culture, Properties.Resources.PageNumber, value);
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
