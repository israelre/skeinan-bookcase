﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Services.Searchers.Filters;
using System.Data.Entity;
using System.Linq;

namespace Skeinan.Bookcase.Services.Searchers {
	public class StudentSearchService : SearcherService<StudentEntity, DisplayableStudent, StudentSearcherFilter> {
		public Session ActiveSession { get; set; }

		public StudentSearchService(Session session, PathService pathService) : base(pathService) {
			ActiveSession = session;
		}

		protected override IQueryable<StudentEntity> Query(BookcaseContext context) {
			IQueryable<StudentEntity> queryable = context.Students.AsNoTracking();
			queryable = queryable.Include(s => s.Loans.Select(l => l.Book.Titles.Select(t => t.Title)));
			queryable = queryable.OrderBy(s => s.StudentId);
			queryable = FilterQuery(queryable);
			return queryable;
		}

		protected IQueryable<StudentEntity> FilterQuery(IQueryable<StudentEntity> queryable) {
			IQueryable<StudentEntity> filtered = queryable;
			
			filtered = HandleFilter(filtered, CurrentSearch.Code, s => s.Code.ToString().Contains(CurrentSearch.Code));
			filtered = HandleFilter(filtered, CurrentSearch.Name, s => s.Name.ToString().Contains(CurrentSearch.Name));
			filtered = HandleFilter(filtered, CurrentSearch.Year, s => s.Year.ToString().Contains(CurrentSearch.Year));
			filtered = HandleFilter(filtered, CurrentSearch.Grade, s => s.Grade.ToString().Contains(CurrentSearch.Grade));
			filtered = HandleFilter(filtered, CurrentSearch.Classroom, s => s.Classroom.ToString().Contains(CurrentSearch.Classroom));
			filtered = HandleFilter(filtered, CurrentSearch.Number, s => s.Number.ToString().Contains(CurrentSearch.Number));
			filtered = HandleFilter(filtered, CurrentSearch.BirthDate, s => s.BirthDate == CurrentSearch.BirthDate);
			filtered = HandleFilter(filtered, CurrentSearch.Transferred, s => s.Transferred == CurrentSearch.Transferred);

			return filtered;
		}

		protected override DisplayableStudent MapResult(StudentEntity result) {
			return DisplayableStudentMapper.Instance.MapToModel(result);
		}
	}
}
