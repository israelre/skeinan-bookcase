﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Services.Searchers.Filters;
using System;
using System.Data.Entity;
using System.Linq;

namespace Skeinan.Bookcase.Services.Searchers {
	public class LoanSearchService : SearcherService<LoanEntity, DisplayableLoan, LoanSearchFilter> {
		public Session ActiveSession { get; set; }

		public LoanSearchService(Session session, PathService pathService) : base(pathService) {
			ActiveSession = session;
		}

		protected override IQueryable<LoanEntity> Query(BookcaseContext context) {
			IQueryable<LoanEntity> queryable = context.Loans;
			queryable = queryable.Include(l => l.Book.Titles.Select(e => e.Title));
			queryable = queryable.Include(l => l.Student);
			queryable = queryable.OrderBy(l => l.LoanId);
			queryable = FilterQuery(queryable);
			return queryable;
		}

		protected IQueryable<LoanEntity> FilterQuery(IQueryable<LoanEntity> queryable) {
			IQueryable<LoanEntity> filtered = queryable;

			filtered = HandleFilter(filtered, CurrentSearch.BookTitle, l => l.Book.Titles.Any(t => t.Title.Name.Contains(CurrentSearch.BookTitle)));

			if (CurrentSearch.LoanDate != null) {
				DateTime beginDate = CurrentSearch.LoanDate.Value.Date;
				DateTime endDate = CurrentSearch.LoanDate.Value.Date.AddDays(1);
				filtered = HandleFilter(filtered, CurrentSearch.LoanDate, l => l.LoanDate > beginDate && l.LoanDate < endDate);
			}

			if (CurrentSearch.ReturnDate != null) {
				DateTime beginDate = CurrentSearch.ReturnDate.Value.Date;
				DateTime endDate = CurrentSearch.ReturnDate.Value.Date.AddDays(1);
				filtered = HandleFilter(filtered, CurrentSearch.ReturnDate, l => l.ReturnDate > beginDate && l.ReturnDate < endDate);
			}

			filtered = HandleFilter(filtered, CurrentSearch.StudentName, l => l.Student.Name.Contains(CurrentSearch.StudentName));

			return filtered;
		}

		protected override DisplayableLoan MapResult(LoanEntity result) {
			return DisplayableLoanMapper.Instance.MapToModel(result);
		}
	}
}
