﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace Skeinan.Bookcase.Services.Searchers {
	public abstract class SearcherService<T, U, V> : Service where V : Model {
		private const int PAGE_SIZE = 100;

		private int mCurrentPage;
		private V mPreviousSearch = Activator.CreateInstance<V>();
		
		public PathService PathService { get; }

		private V mCurrentSearch = Activator.CreateInstance<V>();
		public V CurrentSearch {
			get { return mCurrentSearch; }
			set { mCurrentSearch = value; NotifyPropertyChanged(); }
		}

		private bool mHasPreviousPage;
		public bool HasPreviousPage {
			get { return mHasPreviousPage; }
			set { mHasPreviousPage = value; NotifyPropertyChanged(); }
		}

		private bool mHasNextPage;
		public bool HasNextPage {
			get { return mHasNextPage; }
			set { mHasNextPage = value; NotifyPropertyChanged(); }
		}

		private int mPageNumber;
		public int PageNumber {
			get { return mPageNumber; }
			set { mPageNumber = value; NotifyPropertyChanged(); }
		}

		public BindingList<U> ResultData { get; } = new BindingList<U>();

		public void OnLoad() {
			Search();
		}

		public void Search() {
			using (BookcaseContext context = new BookcaseContext(PathService.DatabasePath)) {
				IQueryable<T> queryable = Query(context);
				PopulateResults(queryable);
			}
		}

		private void PopulateResults(IQueryable<T> queryable) {
			ResultData.RaiseListChangedEvents = false;
			ResultData.Clear();
			mPreviousSearch = (V)CurrentSearch.Clone();
			RestrictPage(queryable);
			foreach (U book in RetrieveResults(queryable)) {
				ResultData.Add(book);
			}
			ResultData.RaiseListChangedEvents = true;
			ResultData.ResetBindings();
			HasPreviousPage = mCurrentPage != 0;
			HasNextPage = queryable.Count() > (mCurrentPage + 1) * PAGE_SIZE;
		}

		private void Page(int page) {
			using (BookcaseContext context = new BookcaseContext(PathService.DatabasePath)) {
				IQueryable<T> queryable = Query(context);
				mCurrentPage = page;
				PopulateResults(queryable);
			}
		}

		private void RestrictPage(IQueryable<T> queryable) {
			int pageCount = CountPages(queryable);
			if (mCurrentPage >= pageCount) {
				mCurrentPage = pageCount - 1;
			}
			if (mCurrentPage < 0) {
				mCurrentPage = 0;
			}
			PageNumber = pageCount == 0 ? 0 : mCurrentPage + 1;
		}

		public void PageNext() {
			Page(mCurrentPage + 1);
		}

		public void PagePrevious() {
			Page(mCurrentPage - 1);
		}

		protected int CountPages(IQueryable<T> queryable) {
			return (queryable.Count() + PAGE_SIZE - 1) / PAGE_SIZE;
		}

		protected List<U> RetrieveResults(IQueryable<T> queryable) {
			queryable = queryable.Skip(mCurrentPage * PAGE_SIZE);
			queryable = queryable.Take(PAGE_SIZE);
			return MapResults(queryable);
		}

		protected abstract IQueryable<T> Query(BookcaseContext context);

		protected List<U> MapResults(IQueryable<T> queryable) {
			List<U> results = new List<U>();
			foreach (T entity in queryable) {
				results.Add(MapResult(entity));
			}
			return results;
		}

		protected abstract U MapResult(T result);

		public void ClearFilters() {
			CurrentSearch = Activator.CreateInstance<V>();
			if (!Util.ArePropertiesEqual(mPreviousSearch, CurrentSearch)) Search();
		}

		public SearcherService(PathService pathService) {
			PathService = pathService;
		}

		protected IQueryable<T> HandleFilter(IQueryable<T> queryable, object search, Expression<Func<T, bool>> expression) {
			if (search != null && !string.IsNullOrWhiteSpace(search.ToString())) {
				queryable = queryable.Where(expression);
			}
			return queryable;
		}
	}
}
