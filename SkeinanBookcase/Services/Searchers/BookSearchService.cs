﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Services.Searchers.Filters;
using System.Data.Entity;
using System.Linq;

namespace Skeinan.Bookcase.Services.Searchers {
	public class BookSearchService : SearcherService<BookEntity, DisplayableBook, BookSearcherFilter> {
		public Session ActiveSession { get; set; }

		public BookSearchService(Session session, PathService pathService) : base(pathService) {
			ActiveSession = session;
		}

		protected override IQueryable<BookEntity> Query(BookcaseContext context) {
			IQueryable<BookEntity> queryable = context.Books;
			queryable = queryable.Include(b => b.Titles.Select(e => e.Title));
			queryable = queryable.Include(b => b.Authors.Select(e => e.Author));
			queryable = queryable.Include(b => b.Classifications.Select(e => e.Classification));
			queryable = queryable.Include(b => b.Publishers.Select(e => e.Publisher));
			queryable = queryable.Include(b => b.Loans);
			queryable = queryable.OrderBy(b => b.BookId);
			queryable = FilterQuery(queryable);
			return queryable;
		}

		protected IQueryable<BookEntity> FilterQuery(IQueryable<BookEntity> queryable) {
			IQueryable<BookEntity> filtered = queryable;

			filtered = HandleFilter(filtered, CurrentSearch.Code, b => b.Code.ToString().Contains(CurrentSearch.Code));
			filtered = HandleFilter(filtered, CurrentSearch.Isbn, b => b.Isbn.ToString().Contains(CurrentSearch.Isbn));
			filtered = HandleFilter(filtered, CurrentSearch.Year, b => b.Year.ToString().Contains(CurrentSearch.Year));
			filtered = HandleFilter(filtered, CurrentSearch.Edition, b => b.Edition.ToString().Contains(CurrentSearch.Edition));
			filtered = HandleFilter(filtered, CurrentSearch.Source, b => b.Source.ToString().Contains(CurrentSearch.Source));
			filtered = HandleFilter(filtered, CurrentSearch.Title, b => b.Titles.Any(t => t.Title.Name.Contains(CurrentSearch.Title)));
			filtered = HandleFilter(filtered, CurrentSearch.Author, b => b.Authors.Any(t => t.Author.Name.Contains(CurrentSearch.Author)));
			filtered = HandleFilter(filtered, CurrentSearch.Classification, b => b.Classifications.Any(t => t.Classification.Name.Contains(CurrentSearch.Classification)));
			filtered = HandleFilter(filtered, CurrentSearch.Publisher, b => b.Publishers.Any(t => t.Publisher.Name.Contains(CurrentSearch.Publisher)));

			return filtered;
		}

		protected override DisplayableBook MapResult(BookEntity result) {
			return DisplayableBookMapper.Instance.MapToModel(result);
		}
	}
}
