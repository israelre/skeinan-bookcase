﻿using Skeinan.Bookcase.Models;
using System;

namespace Skeinan.Bookcase.Services.Searchers.Filters {
	public class LoanSearchFilter : Model {
		private string mId;
		public string Id {
			get { return mId; }
			set { mId = value; NotifyPropertyChanged(); }
		}

		private DateTime? mLoanDate;
		public DateTime? LoanDate {
			get { return mLoanDate; }
			set { mLoanDate = value; NotifyPropertyChanged(); }
		}

		private DateTime? mReturnDate;
		public DateTime? ReturnDate {
			get { return mReturnDate; }
			set { mReturnDate = value; NotifyPropertyChanged(); }
		}

		private string mBookTitle;
		public string BookTitle {
			get { return mBookTitle; }
			set { mBookTitle = value; NotifyPropertyChanged(); }
		}

		private string mStudentName;
		public string StudentName {
			get { return mStudentName; }
			set { mStudentName = value; NotifyPropertyChanged(); }
		}

	}
}
