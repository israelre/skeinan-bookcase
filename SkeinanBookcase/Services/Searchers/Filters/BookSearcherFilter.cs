﻿using Skeinan.Bookcase.Models;

namespace Skeinan.Bookcase.Services.Searchers.Filters {
	public class BookSearcherFilter : Model {
		private string mBookId;
		public string BookId {
			get { return mBookId; }
			set { mBookId = value; NotifyPropertyChanged(); }
		}

		private string mCode;
		public string Code {
			get { return mCode; }
			set { mCode = value; NotifyPropertyChanged(); }
		}

		private string mIsbn;
		public string Isbn {
			get { return mIsbn; }
			set { mIsbn = value; NotifyPropertyChanged(); }
		}

		private string mYear;
		public string Year {
			get { return mYear; }
			set { mYear = value; NotifyPropertyChanged(); }
		}

		private string mEdition;
		public string Edition {
			get { return mEdition; }
			set { mEdition = value; NotifyPropertyChanged(); }
		}

		private string mSource;
		public string Source {
			get { return mSource; }
			set { mSource = value; NotifyPropertyChanged(); }
		}

		private string mAddDate;
		public string AddDate {
			get { return mAddDate; }
			set { mAddDate = value; NotifyPropertyChanged(); }
		}

		private string mTitle;
		public string Title {
			get { return mTitle; }
			set { mTitle = value; NotifyPropertyChanged(); }
		}

		private string mAuthor;
		public string Author {
			get { return mAuthor; }
			set { mAuthor = value; NotifyPropertyChanged(); }
		}

		private string mClassification;
		public string Classification {
			get { return mClassification; }
			set { mClassification = value; NotifyPropertyChanged(); }
		}

		private string mPublisher;
		public string Publisher {
			get { return mPublisher; }
			set { mPublisher = value; NotifyPropertyChanged(); }
		}

		private string mLoanDate;
		public string LoanDate {
			get { return mLoanDate; }
			set { mLoanDate = value; NotifyPropertyChanged(); }
		}
	}
}
