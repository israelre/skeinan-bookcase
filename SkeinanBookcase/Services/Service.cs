﻿using SkeinanWpf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Skeinan.Bookcase.Services {
	public class Service : INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;
		public PropertyChangeManager PropertyChangeManager { get; }

		public void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void AddListener(string propertyName, Action action) {
			PropertyChangeManager.AddListener(propertyName, action);
		}

		public Service() {
			PropertyChangeManager = new PropertyChangeManager(this);
		}
	}
}
