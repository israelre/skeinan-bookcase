﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System.ComponentModel;
using System.Linq;

namespace Skeinan.Bookcase.Services {
	public class PublisherService : Service {
		public Session ActiveSession { get; }
		public PathService PathService { get; }

		public BindingList<Publisher> Publishers { get; } = new BindingList<Publisher>();

		private Publisher mPublisher;
		public Publisher Publisher {
			get { return mPublisher; }
			set { mPublisher = value; NotifyPropertyChanged(); }
		}

		private string mNewName;
		public string NewName {
			get { return mNewName; }
			set { mNewName = value; NotifyPropertyChanged(); }
		}

		public PublisherService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public void OnLoad() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				long? selectedId = Publisher?.PublisherId;
				Util.RefreshCompleteList(Publishers,
						context.Publishers.AsNoTracking().OrderBy(e => e.Name),
						PublisherMapper.Instance.MapToModel);
				Publisher = Publishers.Where(e => e.PublisherId == selectedId).FirstOrDefault();
			}
		}

		public void Save() {
			Publisher.Name = NewName;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				PublisherEntity existing = context.Publishers.Where(e => e.PublisherId == Publisher.PublisherId).FirstOrDefault();
				if (existing != null) {
					PublisherMapper.Instance.MapToEntity(Publisher, existing);
				} else {
					context.Publishers.Add(PublisherMapper.Instance.MapToEntity(Publisher));
				}
				context.SaveChanges();
			}
		}
	}
}
