﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using System.Linq;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Mappers;

namespace Skeinan.Bookcase.Services {
	public class StudentService : Service {
		private Student mStudent;
		public Student Student {
			get { return mStudent; }
			set { mStudent = value; NotifyPropertyChanged(); }
		}

		public Session ActiveSession { get; set; }
		public PathService PathService { get; set; }

		public StudentService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public void OnLoad(long externalStudentId = 0) {
			if (Student == null) {
				using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
					StudentEntity studentEntity = context.Students.AsNoTracking().Where(s => s.StudentId == externalStudentId).FirstOrDefault();
					Student = StudentMapper.Instance.MapToModel(studentEntity);
				}
			}
			if (Student == null) {
				Student = new Student();
			}
		}

		public void Save() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				StudentEntity existing = context.Students.Where(e => e.StudentId == Student.StudentId).FirstOrDefault();
				if (existing != null) {
					StudentMapper.Instance.MapToEntity(Student, existing);
				} else {
					context.Students.Add(StudentMapper.Instance.MapToEntity(Student));
				}
				context.SaveChanges();
			}
		}

		public bool StudentCodeExists(string code) {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				return context.Students.Any(e => e.StudentId != Student.StudentId && e.Code == code);
			}
		}
	}
}
