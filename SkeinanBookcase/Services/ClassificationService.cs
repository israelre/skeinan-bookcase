﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System.ComponentModel;
using System.Linq;

namespace Skeinan.Bookcase.Services {
	public class ClassificationService : Service {
		public Session ActiveSession { get; }
		public PathService PathService { get; }

		public BindingList<Classification> Classifications { get; } = new BindingList<Classification>();

		private Classification mClassification;
		public Classification Classification {
			get { return mClassification; }
			set { mClassification = value; NotifyPropertyChanged(); }
		}

		private string mNewName;
		public string NewName {
			get { return mNewName; }
			set { mNewName = value; NotifyPropertyChanged(); }
		}

		public ClassificationService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public void OnLoad() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				long? selectedId = Classification?.ClassificationId;
				Util.RefreshCompleteList(Classifications,
						context.Classifications.AsNoTracking().OrderBy(e => e.Name),
						ClassificationMapper.Instance.MapToModel);
				Classification = Classifications.Where(e => e.ClassificationId == selectedId).FirstOrDefault();
			}
		}

		public void Save() {
			Classification.Name = NewName;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				ClassificationEntity existing = context.Classifications.Where(e => e.ClassificationId == Classification.ClassificationId).FirstOrDefault();
				if (existing != null) {
					ClassificationMapper.Instance.MapToEntity(Classification, existing);
				} else {
					context.Classifications.Add(ClassificationMapper.Instance.MapToEntity(Classification));
				}
				context.SaveChanges();
			}
		}
	}
}
