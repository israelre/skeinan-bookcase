﻿using System;
using System.IO;

namespace Skeinan.Bookcase.Services {
	public class PathService {
		public string AppDataPath { get; }

		public string DatabasePath {
			get {
				return Path.Combine(AppDataPath, "Database.sdf");
			}
		}

		public PathService(params string[] applicationPath) {
			AppDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Skeinan", "Bookcase");
		}

		public string GenerateAppDataPath(string relativePath) {
			return Path.Combine(AppDataPath, relativePath);
		}
	}
}
