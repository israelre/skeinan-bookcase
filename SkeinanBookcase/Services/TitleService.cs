﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using Skeinan.Bookcase.Utils;
using System.ComponentModel;
using System.Linq;

namespace Skeinan.Bookcase.Services {
	public class TitleService : Service {
		public Session ActiveSession { get; }
		public PathService PathService { get; }

		public BindingList<Title> Titles { get; } = new BindingList<Title>();

		private Title mTitle;
		public Title Title {
			get { return mTitle; }
			set { mTitle = value; NotifyPropertyChanged(); }
		}

		private string mNewName;
		public string NewName {
			get { return mNewName; }
			set { mNewName = value; NotifyPropertyChanged(); }
		}

		public TitleService(Session session, PathService pathService) {
			ActiveSession = session;
			PathService = pathService;
		}

		public void OnLoad() {
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				long? selectedId = Title?.TitleId;
				Util.RefreshCompleteList(Titles,
						context.Titles.AsNoTracking().OrderBy(e => e.Name),
						TitleMapper.Instance.MapToModel);
				Title = Titles.Where(e => e.TitleId == selectedId).FirstOrDefault();
			}
		}

		public void Save() {
			Title.Name = NewName;
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				TitleEntity existing = context.Titles.Where(e => e.TitleId == Title.TitleId).FirstOrDefault();
				if (existing != null) {
					TitleMapper.Instance.MapToEntity(Title, existing);
				} else {
					context.Titles.Add(TitleMapper.Instance.MapToEntity(Title));
				}
				context.SaveChanges();
			}
		}
	}
}
