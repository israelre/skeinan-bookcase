﻿using Skeinan.Bookcase.Data.Configurations;
using Skeinan.Bookcase.Data.Entities;
using Skeinan.Bookcase.Mappers;
using Skeinan.Bookcase.Models;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Skeinan.Bookcase.Services {
	public class LoginService : Service {
		private static readonly Encoding encoding = Encoding.UTF8;
		public Session ActiveSession { get; }
		public PathService PathService { get; }

		public LoginService(PathService pathService) {
			PathService = pathService;
		}

		public void OnLoad() {
			using (BookcaseContext context = new BookcaseContext(PathService.DatabasePath)) {
				context.Database.Initialize(false);
			}
		}

		public User LogIn(string name, string password) {
			UserEntity user;
			using (BookcaseContext context = new BookcaseContext(PathService.DatabasePath)) {
				if (context.Users.Count() == 0) {
					Save(name, password);
				}
				user = context.Users.AsNoTracking().Where(u => u.Name == name).FirstOrDefault();
				if (user != null) {
					byte[] hashedBytes = SHA512.Create().ComputeHash(encoding.GetBytes(password).Concat(Convert.FromBase64String(user.Salt)).ToArray());
					string hashedPassword = encoding.GetString(hashedBytes);
					if (user.HashedPassword != hashedPassword) {
						user = null;
					}
				}
			}
			return user != null ? UserMapper.Instance.MapToModel(user) : null;
		}

		public User Save(string name, string password) {
			byte[] salt = new byte[64];
			RandomNumberGenerator.Create().GetBytes(salt);
			byte[] hashedBytes = SHA512.Create().ComputeHash(encoding.GetBytes(password).Concat(salt).ToArray());

			User user = new User();
			user.Name = name;
			user.Salt = Convert.ToBase64String(salt);
			user.HashedPassword = encoding.GetString(hashedBytes);
			using (BookcaseContext context = new BookcaseContext(ActiveSession, PathService.DatabasePath)) {
				context.Users.Add(UserMapper.Instance.MapToEntity(user));
				context.SaveChanges();
			}
			return user;
		}
	}
}
