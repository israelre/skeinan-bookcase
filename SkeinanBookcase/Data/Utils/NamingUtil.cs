﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Skeinan.Bookcase.Data.Utils {
	class NamingUtil {
		public static string ConvertFieldNameToColumnName(string fieldName) {
			return ConvertCamelCaseToUnderscore(fieldName);
		}

		public static string ConvertToColumnName(PropertyInfo field) {
			return ConvertFieldNameToColumnName(field.Name);
		}

		public static string ConvertClassNameToTableName(string className) {
			string camelCaseTableName = className;
			if (camelCaseTableName.EndsWith("Entity")) {
				camelCaseTableName = camelCaseTableName.Substring(0, className.Length - 6);
			}
			return ConvertCamelCaseToUnderscore(camelCaseTableName);
		}

		public static string ConvertToTableName(Type tableType) {
			return ConvertClassNameToTableName(tableType.Name);
		}

		private static string ConvertCamelCaseToUnderscore(string fieldName) {
			return Regex.Replace(fieldName, "([a-z])([A-Z])", "$1_$2", RegexOptions.CultureInvariant).ToLowerInvariant();
		}

		public static string ComputePrimaryKeyFieldNameFromClassName(string className) {
			string camelCaseTableName = className;
			if (camelCaseTableName.EndsWith("Entity")) {
				camelCaseTableName = camelCaseTableName.Substring(0, className.Length - 6);
			}
			return camelCaseTableName + "Id";
		}

		public static string ComputePrimaryKeyColumnNameFromClassName(string className) {
			return ConvertCamelCaseToUnderscore(ComputePrimaryKeyFieldNameFromClassName(className));
		}
	}
}
