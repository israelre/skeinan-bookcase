﻿using Skeinan.Bookcase.Data.Utils;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Skeinan.Bookcase.Data.Configurations {
	class EntityConvention : IStoreModelConvention<EdmProperty>, IStoreModelConvention<EntitySet>, IStoreModelConvention<AssociationType> {
		public void Apply(EdmProperty item, DbModel model) {
			item.Name = NamingUtil.ConvertFieldNameToColumnName(item.Name);
		}
		
		public void Apply(EntitySet item, DbModel model) {
			item.Table = NamingUtil.ConvertClassNameToTableName(item.Name);
		}

		public void Apply(AssociationType item, DbModel model) {
			ReferentialConstraint constraint = item.Constraint;
			if (item.IsForeignKey && constraint.FromProperties.Count == 1 && constraint.ToProperties.Count == 1) {
				constraint.ToProperties[0].Name = NamingUtil.ComputePrimaryKeyColumnNameFromClassName(constraint.FromProperties[0].DeclaringType.Name);
			}
		}
	}
}
