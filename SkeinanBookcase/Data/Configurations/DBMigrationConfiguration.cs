﻿using System.Data.Entity.Migrations;

namespace Skeinan.Bookcase.Data.Configurations {
	class DBMigrationConfiguration : DbMigrationsConfiguration<BookcaseContext> {
		public DBMigrationConfiguration() {
			AutomaticMigrationsEnabled = true;
		}
	}
}
