﻿using Skeinan.Bookcase.Data.Entities;
using System.Data.Entity;

namespace Skeinan.Bookcase.Data.Configurations {
	class TableJoiner {
		private DbModelBuilder mModelBuilder;
		public TableJoiner(DbModelBuilder modelBuilder) {
			mModelBuilder = modelBuilder;
		}

		public void JoinAll() {
			JoinBookTitle();
			JoinBookAuthor();
			JoinBookClassification();
			JoinBookPublisher();
		}

		private void JoinBookTitle() {
			mModelBuilder.Entity<BookTitleEntity>().HasKey(e => new { e.BookId, e.TitleId });
			mModelBuilder.Entity<BookTitleEntity>()
					.HasRequired(e => e.Book)
					.WithMany(e => e.Titles)
					.HasForeignKey(e => e.BookId);
			mModelBuilder.Entity<BookTitleEntity>()
					.HasRequired(e => e.Title)
					.WithMany(e => e.Books)
					.HasForeignKey(e => e.TitleId);
			mModelBuilder.Entity<BookTitleEntity>()
					.HasIndex(e => e.BookId);
		}

		private void JoinBookAuthor() {
			mModelBuilder.Entity<BookAuthorEntity>().HasKey(e => new { e.BookId, e.AuthorId });
			mModelBuilder.Entity<BookAuthorEntity>()
					.HasRequired(e => e.Book)
					.WithMany(e => e.Authors)
					.HasForeignKey(e => e.BookId);
			mModelBuilder.Entity<BookAuthorEntity>()
					.HasRequired(e => e.Author)
					.WithMany(e => e.Books)
					.HasForeignKey(e => e.AuthorId);
			mModelBuilder.Entity<BookAuthorEntity>()
					.HasIndex(e => e.BookId);
		}

		private void JoinBookClassification() {
			mModelBuilder.Entity<BookClassificationEntity>().HasKey(e => new { e.BookId, e.ClassificationId });
			mModelBuilder.Entity<BookClassificationEntity>()
					.HasRequired(e => e.Book)
					.WithMany(e => e.Classifications)
					.HasForeignKey(e => e.BookId);
			mModelBuilder.Entity<BookClassificationEntity>()
					.HasRequired(e => e.Classification)
					.WithMany(e => e.Books)
					.HasForeignKey(e => e.ClassificationId);
			mModelBuilder.Entity<BookClassificationEntity>()
					.HasIndex(e => e.BookId);
		}

		private void JoinBookPublisher() {
			mModelBuilder.Entity<BookPublisherEntity>().HasKey(e => new { e.BookId, e.PublisherId });
			mModelBuilder.Entity<BookPublisherEntity>()
					.HasRequired(e => e.Book)
					.WithMany(e => e.Publishers)
					.HasForeignKey(e => e.BookId);
			mModelBuilder.Entity<BookPublisherEntity>()
					.HasRequired(e => e.Publisher)
					.WithMany(e => e.Books)
					.HasForeignKey(e => e.PublisherId);
			mModelBuilder.Entity<BookPublisherEntity>()
					.HasIndex(e => e.BookId);
		}
	}
}
