﻿using Skeinan.Bookcase.Data.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Linq;
using System;
using System.Collections.Generic;
using Skeinan.Bookcase.Data.Utils;
using System.Diagnostics;

namespace Skeinan.Bookcase.Data.Configurations {
	[DbConfigurationType(typeof(DatabaseConfiguration))]
	public class BookcaseContext : DbContext {
		public Session ActiveSession { get; }
		
		public BookcaseContext(string connectionString) : base(connectionString) {
		}

		public BookcaseContext(Session session, string connectionString) : base(connectionString) {
			ActiveSession = session;
		}

		public DbSet<UserEntity> Users { get; set; }
		public DbSet<BookEntity> Books { get; set; }
		public DbSet<StudentEntity> Students { get; set; }
		public DbSet<TitleEntity> Titles { get; set; }
		public DbSet<AuthorEntity> Authors { get; set; }
		public DbSet<ClassificationEntity> Classifications { get; set; }
		public DbSet<PublisherEntity> Publishers { get; set; }
		public DbSet<LoanEntity> Loans { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			modelBuilder.Conventions.Add<EntityConvention>();
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Types().Where(e => !IsValidTable(e)).Configure(e => e.Ignore());

			modelBuilder.Properties().Where(IsIdColumn).Configure(UpdateIdColumnData);

			TableJoiner tableJoiner = new TableJoiner(modelBuilder);
			tableJoiner.JoinAll();
		}

		private bool IsValidTable(Type table) {
			return typeof(Entity).IsAssignableFrom(table);
		}

		private bool IsIdColumn(PropertyInfo property) {
			return property.Name == NamingUtil.ComputePrimaryKeyFieldNameFromClassName(property.DeclaringType.Name);
		}

		private void UpdateIdColumnData(ConventionPrimitivePropertyConfiguration configuration) {
			configuration.IsKey();
			configuration.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
			configuration.IsRequired();
		}

		public override int SaveChanges() {
			ClearEmptyStrings();

			ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;

			IEnumerable<ObjectStateEntry> pendingEntries = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Added | EntityState.Unchanged);
			foreach (ObjectStateEntry entry in pendingEntries) {
				if (entry.State == EntityState.Added) {
					((Entity)entry.Entity).CreationDate = DateTime.Now;
					((Entity)entry.Entity).CreationUser = ActiveSession?.User?.Name;
				}
				((Entity)entry.Entity).LastChangeDate = DateTime.Now;
				((Entity)entry.Entity).LastChangeUser = ActiveSession?.User?.Name;
			}

			return base.SaveChanges();
		}

		private void ClearEmptyStrings() {
			ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;
			IEnumerable<ObjectStateEntry> entries = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified);
			foreach (ObjectStateEntry entry in entries) {
				CurrentValueRecord values = entry.CurrentValues;
				for (int i = 0; i < values.FieldCount; ++i) {
					object value = values[i];
					bool isString = values.GetDataTypeName(i) == "String";
					if (isString && !values.IsDBNull(i) && values.GetString(i).All(char.IsWhiteSpace)) {
						values.SetValue(i, null);
					}
				}
			}
		}

		public void EnableLog() {
			Database.Log = s => Debug.WriteLine(s);
		}
	}
}
