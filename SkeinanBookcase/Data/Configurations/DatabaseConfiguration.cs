﻿using EFCache;
using System.Data.Entity;
using System.Data.Entity.Core.Common;

namespace Skeinan.Bookcase.Data.Configurations {
	class DatabaseConfiguration : DbConfiguration {
		internal static readonly InMemoryCache Cache = new InMemoryCache();
		public DatabaseConfiguration() {
			var transactionHandler = new CacheTransactionHandler(Cache);

			AddInterceptor(transactionHandler);

			var cachingPolicy = new CachingPolicy();

			Loaded +=
					(sender, args) => args.ReplaceService<DbProviderServices>(
					(s, _) => new CachingProviderServices(s, transactionHandler, cachingPolicy));
		}
	}
}
