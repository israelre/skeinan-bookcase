﻿namespace Skeinan.Bookcase.Data.Entities {
	public class BookClassificationEntity : Entity {
		public long BookId { get; set; }
		public long ClassificationId { get; set; }
		public virtual BookEntity Book { get; set; }
		public virtual ClassificationEntity Classification { get; set; }
	}
}
