﻿using System;

namespace Skeinan.Bookcase.Data.Entities {
	public class LoanEntity : Entity {
		public long LoanId { get; set; }
		public long BookId { get; set; }
		public long StudentId { get; set; }
		public virtual StudentEntity Student { get; set; }
		public virtual BookEntity Book { get; set; }
		public DateTime LoanDate { get; set; }
		public DateTime? ReturnDate { get; set; }
	}
}
