﻿namespace Skeinan.Bookcase.Data.Entities {
	public class BookAuthorEntity : Entity {
		public long BookId { get; set; }
		public long AuthorId { get; set; }
		public virtual BookEntity Book { get; set; }
		public virtual AuthorEntity Author { get; set; }
	}
}
