﻿using System;
using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public partial class BookEntity : Entity {
		public long BookId { get; set; }
		public string Code { get; set; }
		public string Isbn { get; set; }
		public int? Year { get; set; }
		public int? Edition { get; set; }
		public string Source { get; set; }
		public DateTime? AddDate { get; set; }
		public virtual ICollection<BookTitleEntity> Titles { get; set; } = new List<BookTitleEntity>();
		public virtual ICollection<BookAuthorEntity> Authors { get; set; } = new List<BookAuthorEntity>();
		public virtual ICollection<BookClassificationEntity> Classifications { get; set; } = new List<BookClassificationEntity>();
		public virtual ICollection<BookPublisherEntity> Publishers { get; set; } = new List<BookPublisherEntity>();
		public virtual ICollection<LoanEntity> Loans { get; set; } = new List<LoanEntity>();
	}
}
