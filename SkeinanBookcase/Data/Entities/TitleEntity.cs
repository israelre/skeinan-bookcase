﻿using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class TitleEntity : Entity {
		public long TitleId { get; set; }
		public string Name { get; set; }
		public virtual ICollection<BookTitleEntity> Books { get; set; }
	}
}
