﻿namespace Skeinan.Bookcase.Data.Entities {
	public class BookPublisherEntity : Entity {
		public long BookId { get; set; }
		public long PublisherId { get; set; }
		public virtual BookEntity Book { get; set; }
		public virtual PublisherEntity Publisher { get; set; }
	}
}
