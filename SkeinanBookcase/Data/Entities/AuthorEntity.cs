﻿using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class AuthorEntity : Entity {
		public long AuthorId { get; set; }
		public string Name { get; set; }
		public virtual ICollection<BookAuthorEntity> Books { get; set; }
	}
}
