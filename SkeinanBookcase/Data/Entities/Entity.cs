﻿using System;

namespace Skeinan.Bookcase.Data.Entities {
	public class Entity {
		public DateTime CreationDate { get; set; } = DateTime.Now;
		public string CreationUser { get; set; } = "Unknown";
		public DateTime LastChangeDate { get; set; } = DateTime.Now;
		public string LastChangeUser { get; set; } = "Unknown";
	}
}
