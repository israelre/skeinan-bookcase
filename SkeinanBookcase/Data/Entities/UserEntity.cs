﻿using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class UserEntity : Entity {
		public long UserId { get; set; }
		public string Name { get; set; }
		public string HashedPassword { get; set; }
		public string Salt { get; set; }
	}
}
