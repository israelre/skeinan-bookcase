﻿using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class PublisherEntity : Entity {
		public long PublisherId { get; set; }
		public string Name { get; set; }
		public virtual ICollection<BookPublisherEntity> Books { get; set; }
	}
}
