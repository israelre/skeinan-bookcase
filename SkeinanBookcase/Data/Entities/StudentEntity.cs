﻿using System;
using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class StudentEntity : Entity {
		public long StudentId { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Registry { get; set; }
		public bool Transferred { get; set; }
		public int? Year { get; set; }
		public int? Grade { get; set; }
		public string Classroom { get; set; }
		public int? Number { get; set; }
		public DateTime? BirthDate { get; set; }
		public virtual ICollection<LoanEntity> Loans { get; set; }
	}
}
