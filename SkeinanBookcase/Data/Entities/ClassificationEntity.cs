﻿using System.Collections.Generic;

namespace Skeinan.Bookcase.Data.Entities {
	public class ClassificationEntity : Entity {
		public long ClassificationId { get; set; }
		public string Name { get; set; }
		public virtual ICollection<BookClassificationEntity> Books { get; set; }
	}
}
