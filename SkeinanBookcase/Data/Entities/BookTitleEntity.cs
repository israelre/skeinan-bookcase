﻿namespace Skeinan.Bookcase.Data.Entities {
	public class BookTitleEntity : Entity {
		public long BookId { get; set; }
		public long TitleId { get; set; }
		public bool IsPrimary { get; set; }
		public virtual BookEntity Book { get; set; }
		public virtual TitleEntity Title { get; set; }
	}
}
