# Skeinan Bookcase

This application is used to manage books and loans in libraries.

## Development Instructions

The Skeinan NuGet package source needs to be added to install the `skeinan-wpf` package:
https://pkgs.dev.azure.com/skeinan/Skeinan/_packaging/skeinan/nuget/v3/index.json

The `Microsoft Visual Studio Installer Projects` extension is required to run the Installer projects.
These projects are not required unless an installer is needed.

## Application Usage Instructions

The database is stored in `%AppLocalData%/Skeinan`, so all application data can be cleared by deleting this folder.

Manually creating and managing users is currently unsupported.
The first attempt to login with an empty database will create a user with the entered username and password. Any future changes need to be done directly in the database.
